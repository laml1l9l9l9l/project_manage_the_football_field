<?php

// Route::get("", "Admin\Controller@layer");

Route::get("", "Admin\Controller@view_login")
->name("admin.view_login");
Route::post("process_login", "Admin\Controller@process_login")
->name("process_login");


Route::group(["middleware" => "CheckAdmin"], function(){
	Route::get("wellcome", "Admin\Controller@wellcome")
	->name("wellcome");
	Route::get("Calendar", "Admin\Controller@calendar")
	->name("Calendar");
	
	Route::group(["prefix" => "admin"],function(){
		Route::get("logout", "Admin\Controller@logout")
		->name("logout");
		Route::get("view_all", "Admin\AdminController@view_all")
		->name("admin.view_all");
		Route::get("view_insert", "Admin\AdminController@view_insert")
		->name("admin.view_insert");
		Route::post("process_insert", "Admin\AdminController@process_insert")
		->name("admin.process_insert");
		Route::get("view_update/{id}", "Admin\AdminController@view_update")
		->name("admin.view_update");
		Route::post("process_update/{id}", "Admin\AdminController@process_update")
		->name("admin.process_update");
		Route::get("process_delete/{id}", "Admin\AdminController@process_delete")
		->name("admin.process_delete");
	});

	Route::group(["prefix" => "san_bong"],function(){
		Route::get("view_all", "Admin\SanBongController@view_all")
		->name("san_bong.view_all");
		Route::get("view_insert", "Admin\SanBongController@view_insert")
		->name("san_bong.view_insert");
		Route::post("process_insert", "Admin\SanBongController@process_insert")
		->name("san_bong.process_insert");
		Route::get("view_update/{id}", "Admin\SanBongController@view_update")
		->name("san_bong.view_update");
		Route::post("process_update/{id}", "Admin\SanBongController@process_update")
		->name("san_bong.process_update");
		// Route::get("process_delete/{id}", "Admin\SanBongController@process_delete")
		// ->name("san_bong.process_delete");
	});	

	Route::group(["prefix" => "khach_hang"],function(){
		Route::get("view_all", "Admin\KhachHangController@view_all")
		->name("khach_hang.view_all");
		// Route::get("view_insert", "Admin\KhachHangController@view_insert")
		// ->name("san_bong.view_insert");
		// Route::post("process_insert", "Admin\KhachHangController@process_insert")
		// ->name("san_bong.process_insert");
		// Route::get("view_update/{id}", "Admin\AdminController@view_update")
		// ->name("admin.view_update");
		// Route::post("process_update/{id}", "Admin\AdminController@process_update")
		// ->name("admin.process_update");
		// Route::get("process_delete/{id}", "Admin\AdminController@process_delete")
		// ->name("admin.process_delete");
	});	

	Route::group(["prefix" => "hoa_don"],function(){
		Route::get("view_all", "Admin\HoaDonController@view_all")
		->name("hoa_don.view_all");
	});
});