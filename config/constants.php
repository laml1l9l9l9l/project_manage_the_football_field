<?php
	return [
	    'tinh_trang_hoa_don' => [
			'da_thanh_toan'   => '1',
			'chua_thanh_toan' => '0',
			'da_dat_coc'      => '-1',
			'chua_dat_coc'    => '-2',
	    ],

	    'chon_khung_gio_dat_lich' => [
			'khong_duoc_chon' => '0',
			'duoc_chon'       => '1',
	    ],

	    'dac_biet_ngay' => [
			'khong' => '0',
			'co'    => '1',
	    ],

	    'nghi_ngay' => [
			'khong' => '0',
			'co'    => '1',
	    ],

	    'loai_san_bong' => [
			'san_7'  => '0',
			'san_9'  => '1',
			'san_11' => '2',
	    ],
	];