<?php

namespace App\Models\Khach_hang;

use DB;

class san_bong_model
{
	private $table = "san_bong";
	public $ma_san_bong;
	public $loai_san_bong;
	public $gia;
	public $anh;

	public function get_all()
	{
		$array_san_bong = DB::table($this->table)
			->where('ma_san_bong', 'like', "%$this->ma_san_bong%")
			->get();
		return $array_san_bong;
	}

	// public function insert()
	// {
	// 	DB::insert("INSERT into $this->table(ho,ten,nam_sinh,gioi_tinh,ma_lop) values (?,?,?,?,?)",[
	// 		$this->ho,
	// 		$this->ten,
	// 		$this->nam_sinh,
	// 		$this->gioi_tinh,
	// 		$this->ma_lop
	// 	]);
	// }

	public function get_one()
	{
		// lấy sân còn khung giờ trông ngày hôm nay - kiểm tra các khung giờ đã được đặt hết chưa
		$array_san_bong = DB::select("SELECT * from $this->table
			where ma_san_bong = ?
			limit 1",[
				$this->ma_san_bong,
			]);
		return $array_san_bong[0];
	}

	public function get_detail_football_ground()
	{
		// lấy ra sân theo ngày hiện tại để xem có tăng giá hay ko?
		$array = DB::table($this->table)
            // ->join("hoa_don", "hoa_don.ma_hoa_don", "=", "chi_tiet_hoa_don.ma_hoa_don")
            // ->join("chi_tiet_hoa_don", "hoa_don.ma_hoa_don", "=", "chi_tiet_hoa_don.ngay_da")
            // ->join("ngay", "$this->table.ma_hoa_don", "=", "chi_tiet_hoa_don.ma_hoa_don")
            // ->select("$this->table.*", "chi_tiet_hoa_don.ngay_da", "khung_gio_dat_lich.khung_gio")
            ->get();
		return $array;
	}

	public function check_ma_san_bong()
	{
		$array = DB::table($this->table)
            ->select('ma_san_bong')
            ->get();
		return $array;
	}
}