<?php

namespace App\Models\Khach_hang;

use DB;

class hoa_don_chi_tiet_model
{
	private $table = "chi_tiet_hoa_don";
	public $ma_hoa_don;
	public $ma_san_bong;
	public $ma_khung_gio;
	public $ngay_da;
	public $gia;

	// public function get_all()
	// {
	// 	$array = DB::select("SELECT * from $this->table
	// 		where chon = ?",[
	// 			$this->chon
	// 		]);
	// 	return $array;
	// }

	public function insert()
	{
		DB::table($this->table)->insert(
		    [
				'ma_hoa_don'   => $this->ma_hoa_don,
				'ma_san_bong'  => $this->ma_san_bong,
				'ma_khung_gio' => $this->ma_khung_gio,
				'ngay_da'      => $this->ngay_da,
				'gia'          => $this->gia,
		    ]
		);
	}

	public function get_bill_for_date()
	{
		$array = DB::table($this->table)
			->where('ngay_da', $this->ngay_da)
			->select('ma_hoa_don','ma_san_bong','ma_khung_gio')
			->get();
		return $array;
	}
}