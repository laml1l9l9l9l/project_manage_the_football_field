<?php

namespace App\Models\Khach_hang;

use DB;

class hoa_don_model
{
	private $table = "hoa_don";
	public $ma_hoa_don;
	public $ma_san_bong;
	public $tien_dat_coc;
	public $thanh_tien;
	public $tinh_trang;
	public $ngay_tao;
	public $ma_khach_hang;

	// Cot bang hoa_don_chi_tiet
	public $ngay_da;

	public function get_all()
	{
		$array = DB::select("SELECT * from $this->table");
		return $array;
	}

	public function get_info_bill()
	{
		$array = DB::table($this->table)
            ->join("chi_tiet_hoa_don", "$this->table.ma_hoa_don", "chi_tiet_hoa_don.ma_hoa_don")
            ->join("khung_gio_dat_lich", "khung_gio_dat_lich.ma_khung_gio", "chi_tiet_hoa_don.ma_khung_gio")
            ->join("khach_hang", "$this->table.ma_khach_hang", "khach_hang.ma_khach_hang")
            ->where("$this->table.ma_khach_hang", "$this->ma_khach_hang")
            ->orderBy("$this->table.ngay_tao", "desc")
            ->select("$this->table.*", "chi_tiet_hoa_don.ngay_da", "chi_tiet_hoa_don.ma_san_bong", "khung_gio_dat_lich.*", "khach_hang.ten_khach_hang", "khach_hang.so_dien_thoai")
            ->get();
		return $array;
	}



	public function load_calendar_for_bill()
	{
		$array = DB::table($this->table)
            ->join("chi_tiet_hoa_don", "$this->table.ma_hoa_don", "chi_tiet_hoa_don.ma_hoa_don")
            ->join("khung_gio_dat_lich", "khung_gio_dat_lich.ma_khung_gio", "chi_tiet_hoa_don.ma_khung_gio")
            ->select("$this->table.*", "chi_tiet_hoa_don.ngay_da", "chi_tiet_hoa_don.ma_san_bong", "khung_gio_dat_lich.*")
            ->get();
		return $array;
	}

	public function load_calendar_detail_football_ground_for_bill()
	{
		$array = DB::table($this->table)
            ->join("chi_tiet_hoa_don", "$this->table.ma_hoa_don", "chi_tiet_hoa_don.ma_hoa_don")
            ->join("khung_gio_dat_lich", "khung_gio_dat_lich.ma_khung_gio", "chi_tiet_hoa_don.ma_khung_gio")
            ->select("$this->table.*", "chi_tiet_hoa_don.ngay_da", "khung_gio_dat_lich.*")
            ->where("chi_tiet_hoa_don.ma_san_bong", "$this->ma_san_bong")
            ->get();
		return $array;
	}



	public function insert()
	{
		$ma_hoa_don = DB::table($this->table)->insertGetId(
		    [
				'tien_dat_coc'  => $this->tien_dat_coc,
				'thanh_tien'    => $this->thanh_tien,
				'tinh_trang'    => $this->tinh_trang,
				'ngay_tao'      => $this->ngay_tao,
				'ma_khach_hang' => $this->ma_khach_hang,
		    ]
		);
		
		return $ma_hoa_don;
	}

	// public function get_one()
	// {
	// 	$array_san_bong = DB::select("SELECT * from $this->table join lop 
	// 		on $this->table.ma_lop = lop.ma_lop 
	// 		where ma_san_bong = '$this->ma_san_bong'");
	// 	return $array_san_bong;
	// }
	
	public function check_khung_gio_dat_lich()
	{
		$array_check_khung_gio_dat_lich = DB::table($this->table)
            ->join("chi_tiet_hoa_don", "$this->table.ma_hoa_don", "chi_tiet_hoa_don.ma_hoa_don")
            ->select("chi_tiet_hoa_don.ma_khung_gio","chi_tiet_hoa_don.ma_san_bong")
            ->where("chi_tiet_hoa_don.ngay_da", "$this->ngay_da")
            ->get();

		return $array_check_khung_gio_dat_lich;
	}
}