<?php

namespace App\Models\Khach_hang;

use DB;

class khung_gio_dat_lich_model
{
	private $table = "khung_gio_dat_lich";
	public $ma_khung_gio;
	public $khung_gio;
	public $cap_so_nhan;
	public $chon;

	public function get_all()
	{
		$array = DB::select("SELECT * from $this->table
			where chon = ?",[
				$this->chon
			]);
		return $array;
	}

	// public function insert()
	// {
	// 	DB::insert("INSERT into $this->table(ho,ten,nam_sinh,gioi_tinh,ma_lop) values (?,?,?,?,?)",[
	// 		$this->ho,
	// 		$this->ten,
	// 		$this->nam_sinh,
	// 		$this->gioi_tinh,
	// 		$this->ma_lop
	// 	]);
	// }

	public function get_khung_gio()
	{
		$array_khung_gio = DB::table($this->table)
			->where('ma_khung_gio',$this->ma_khung_gio)
			->select('khung_gio')
			->first();
		return $array_khung_gio;
	}
}