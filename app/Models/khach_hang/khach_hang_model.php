<?php

namespace App\Models\Khach_hang;

use DB;

class khach_hang_model
{
	private $table = "khach_hang";
	public $ma_khach_hang;
	public $tai_khoan_khach_hang;
	public $mat_khau;
	public $ten_khach_hang;
	public $email_khach_hang;
	public $so_dien_thoai;

	public function get_one()
	{
		$array = DB::select("SELECT * from $this->table
			where tai_khoan_khach_hang = ? and mat_khau = ?
			limit 1",[
				$this->tai_khoan_khach_hang,
				$this->mat_khau
			]);
		return $array;
	}

	public function insert_one()
	{
		$array = DB::select("INSERT into 
			$this->table(
			tai_khoan_khach_hang, mat_khau, ten_khach_hang, email_khach_hang, so_dien_thoai)
			values(?,?,?,?,?)",[
				$this->tai_khoan_khach_hang,
				$this->mat_khau,
				$this->ten_khach_hang,
				$this->email_khach_hang,
				$this->so_dien_thoai
			]);
		return $array;
	}

	public function get_detail_once()
	{
		$array = DB::select("SELECT * from $this->table
			where ma_khach_hang = ?
			limit 1",[
				$this->ma_khach_hang
			]);
		return $array;
	}

	public function update_profile()
	{
		DB::insert("UPDATE $this->table
			set email_khach_hang = ?, so_dien_thoai = ?
			where ma_khach_hang  = ? ",[
			$this->email_khach_hang,
			$this->so_dien_thoai,
			$this->ma_khach_hang
		]);
	}

	// public function get_one()
	// {
	// 	$array_san_bong = DB::select("SELECT * from $this->table join lop 
	// 		on $this->table.ma_lop = lop.ma_lop 
	// 		where ma_san_bong = '$this->ma_san_bong'");
	// 	return $array_san_bong;
	// }
}