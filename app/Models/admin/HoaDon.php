<?php

namespace App\Models\Admin;

use DB;

Class HoaDon
{
	private $table = "hoa_don";
	public $ma_hoa_don;
	public $tien_dat_coc;
	public $thanh_tien;
	public $tinh_trang;
	public $ngay_tao;
	public $ma_khach_hang;

	public function get_all()
	{
		$array = DB::select("select * from $this->table");
		return $array;
	}
}