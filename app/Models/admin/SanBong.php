<?php 

namespace App\Models\Admin;

use DB;

class SanBong
{
	private $table = 'san_bong';
	public $ma_san_bong;
	public $loai_san_bong;
	public $gia;
	public $anh;
	public function get_all()
	{
		$array = DB::select("select * from $this->table");
		return $array;
	}

	public function insert()
	{
		DB::table($this->table)->insert(
		    [
				'loai_san_bong' => $this->loai_san_bong,
				'gia'           => $this->gia,
				'anh'           => $this->anh
		    ]
		);
	}

	public function get_one()
	{
		$array = DB::table($this->table)
			->where('ma_san_bong',$this->ma_san_bong)
			->first();
		return $array;
	}

	public function update()
	{
		DB::table($this->table)
			->where('ma_san_bong', $this->ma_san_bong)
            ->update([
				'loai_san_bong' => $this->loai_san_bong,
				'gia'           => $this->gia
           	]);
	}
}	