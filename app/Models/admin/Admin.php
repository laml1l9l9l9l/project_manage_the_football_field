<?php 

namespace App\Models\Admin;

use DB;

class Admin
{
	private $table = 'admin';
	public $ma_admin;
	public $mat_khau;
	public $ten_admin;
	public $email_admin;
	public $so_dien_thoai;
	public $phan_quyen;

	public function get_all()
	{
		$array = DB::select("select * from $this->table");
		return $array;
	}

	public function insert()
	{
		DB::insert("insert into $this->table(mat_khau, ten_admin, email_admin, so_dien_thoai, phan_quyen)
			values(?,?,?,?,?)",[
				$this->mat_khau,
				$this->ten_admin,
				$this->email_admin,
				$this->so_dien_thoai,
				$this->phan_quyen
			]);
	}

	public function get_one()
	{
		$array = DB::select("select * from $this->table where ma_admin = ? limit 1",[
			$this->ma_admin
		]);
		return $array[0];
	}

	public function update()
	{
		DB::update("update $this->table set mat_khau = ?, ten_admin = ?, email_admin = ?, 
			so_dien_thoai = ?, phan_quyen = ? where ma_admin = ?",[
					$this->mat_khau,
					$this->ten_admin,
					$this->email_admin,
					$this->so_dien_thoai,
					$this->phan_quyen,
					$this->ma_admin
				]);
	}

	public function delete()
	{
		DB::delete("delete from $this->table where ma_admin = ?",[
			$this->ma_admin
		]);
	}

	public function login()
	{
		$array = DB::select("select * from $this->table
			where email_admin = ? and mat_khau = ?
			limit 1",[
				$this->email_admin,
				$this->mat_khau
			]);
		return $array;
	}
}