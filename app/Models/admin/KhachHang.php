<?php

namespace App\Models\Admin;

use DB;

Class KhachHang
{
	private $table = "khach_hang";
	public $ma_khach_hang;
	public $tai_khoan_khach_hang;
	public $mat_khau;
	public $ten_khach_hang;
	public $email_khach_hang;
	public $so_dien_thoai;

	public function get_all()
	{
		$array = DB::select("select * from $this->table");
		return $array;
	}
}