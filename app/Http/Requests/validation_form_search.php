<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validation_form_search extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    // check validation
    public function rules()
    {
        return [
            'ngay_da' => 'date_format:d/m/Y|after_or_equal:today'
        ];
    }

    // messages
    public function messages()
    {
        return [
            'date_format'    => ':attribute sai định dạng, định dạng chuẩn 31/01/2019',
            'after_or_equal' => ':attribute không được chọn ngày trong quá khứ',
        ];
    }

    // attributes
    public function attributes()
    {
        return [
            'ngay_da' => 'Ngày đá',
        ];
    }

}
