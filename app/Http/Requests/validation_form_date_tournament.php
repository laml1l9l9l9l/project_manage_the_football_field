<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validation_form_date_tournament extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    // check validation
    public function rules()
    {
        return [
            'date_start' => 'date_format:d/m/Y|after:today',
            'date_end'   => 'date_format:d/m/Y|after:date_start'
        ];
    }

    // messages
    public function messages()
    {
        return [
            'date_format' => ':attribute sai định dạng, định dạng chuẩn 01/01/2019',
            'after'       => ':attribute phải chọn sau ngày hiện tại',
        ];
    }

    // attributes
    public function attributes()
    {
        return [
            'date_start' => 'Ngày bắt đầu',
            'date_end'   => 'Ngày kết thúc',
        ];
    }

}
