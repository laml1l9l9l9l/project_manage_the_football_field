<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validation_form_register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    // check validation
    public function rules()
    {
        return [
            'tai_khoan_khach_hang' => 'required|min:5|max:255',
            'ten_khach_hang'       => 'required|string|nullable|min:5|max:255',
            'mat_khau'             => 'required|min:5|max:255',
            'nhap_lai_ma_khau'     => 'required|min:5|max:255',
            'so_dien_thoai'        => 'required|digits:10|phone_number',
            'email_khach_hang'     => 'required|email|min:5|max:255',
        ];
    }

    // messages
    public function messages()
    {
        return [
            'required'     => ':attribute không được để trống',
            'min'          => ':attribute không được nhỏ hơn :min ký tự',
            'max'          => ':attribute không được lớn hơn :max ký tự',
            'digits'       => ':attribute chỉ được nhập 10 số',
            'string'       => ':attribute chỉ được nhập chữ cái',
            'email'        => ':attribute không đúng định dạng',
            'phone_number' => ':attribute bắt đầu bằng số 0',
        ];
    }

}
