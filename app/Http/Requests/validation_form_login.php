<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class validation_form_login extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
    // check validation
    public function rules()
    {
        return [
            'tai_khoan' => 'required|min:5|max:255',
            'mat_khau'  => 'required|min:5|max:255',
        ];
    }

    // messages
    public function messages()
    {
        return [
            'required' => ':attribute không được để trống',
            'min'      => ':attribute không được nhỏ hơn :min ký tự',
            'max'      => ':attribute không được lớn hơn :max ký tự',
        ];
    }

}
