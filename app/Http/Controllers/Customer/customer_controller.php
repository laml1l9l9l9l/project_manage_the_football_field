<?php

namespace App\Http\Controllers\Customer;

use Helpers;
use App\Http\Controllers\Customer\controller;

use Request;
use Illuminate\Http\Request as Requests;
use App\Http\Requests\validation_form_login;
use App\Http\Requests\validation_form_register;
use App\Http\Requests\validation_form_create_bill_once_football_ground;
use App\Http\Requests\validation_form_date;
use App\Http\Requests\validation_form_date_tournament;
use App\Http\Requests\validation_form_search;

use Session;
use App\Models\khach_hang\san_bong_model;
use App\Models\khach_hang\khach_hang_model;
use App\Models\khach_hang\hoa_don_model;
use App\Models\khach_hang\hoa_don_chi_tiet_model;
use App\Models\khach_hang\khung_gio_dat_lich_model;

class customer_controller extends Controller
{
	// Trang chủ
	public function home()
    {
        $class_body = "blog-posts";
        
        $view       = "";

        if ( !empty(Request::get('view')) )
        {
            $view = Request::get('view');
        }
        
        $san_bong       = new san_bong_model();
        $array_san_bong = $san_bong->get_all();

        return view('customer.home',[
            'class_body'     => $class_body,
            'view'           => $view,
            'array_san_bong' => $array_san_bong
        ]);
    }



    // Đăng nhập
    public function view_login()
    {
        $class_body = "login-page";

        if( Session::get('ma_khach_hang') && Session::get('ten_khach_hang') )
        {
            return redirect()->route('customer.home');
        }

        return view('customer.view_login',[
            'class_body' => $class_body
        ]);
    }

    // Xử lý đăng nhập
    public function process_login(validation_form_login $request)
    {
        $khach_hang                       = new khach_hang_model();
        $khach_hang->tai_khoan_khach_hang = $request->input('tai_khoan');
        $khach_hang->mat_khau             = md5($request->input('mat_khau'));

        $khach_hang                       = $khach_hang->get_one();


        if (count($khach_hang) == 1) {
            // Đăng nhập thành công tạo session
            Session::put('ma_khach_hang',$khach_hang[0]->ma_khach_hang);
            Session::put('ten_khach_hang',$khach_hang[0]->ten_khach_hang);

            // Nếu tồn tại session('ma_san_bong'), session('ngay_thue'), session('ma_khung_gio') thì điều hướng về trang xác nhận thông tin tạo sân
            if(Session::has('ma_san_bong') && Session::has('ngay_thue') && Session::has('ma_khung_gio'))
            {
                return redirect()->route('customer.view_create_bill_once_football_ground');
            }
            else
            {
                return redirect()->route('customer.home');
            }
        }

        return redirect()->route('customer.view_login')->with('error','Đăng nhập thất bại');
    }

    // Xử lý đăng xuất
    public function logout()
    {
        Session::flush();

        return redirect()->route('customer.view_login')->with('success','Đăng xuất thành công');
    }



    // Đăng ký
    public function view_register()
    {
        $class_body = "signup-page";

        return view('customer.view_register',[
            'class_body' => $class_body
        ]);
    }

    // Xử lý đăng ký
    public function process_register(validation_form_register $request)
    {
        $khach_hang                       = new khach_hang_model();
        $khach_hang->tai_khoan_khach_hang = Request::get('tai_khoan_khach_hang');
        $khach_hang->ten_khach_hang       = Request::get('ten_khach_hang');
        $khach_hang->mat_khau             = md5(Request::get('mat_khau'));
        $khach_hang->nhap_lai_ma_khau     = Request::get('nhap_lai_ma_khau');
        $khach_hang->so_dien_thoai        = Request::get('so_dien_thoai');
        $khach_hang->email_khach_hang     = Request::get('email_khach_hang');

        if($khach_hang->mat_khau == $khach_hang->nhap_lai_ma_khau)
        {
            $khach_hang                       = $khach_hang->insert_one();

            Session::put('ma_khach_hang',$khach_hang[0]->ma_khach_hang);
            Session::put('ten_khach_hang',$khach_hang[0]->ten_khach_hang);
            return redirect()->route('customer.view_profile')->with('success','Đăng ký thành công');
        }
        else
        {
            return redirect()->route('customer.view_register')->with('error','Mật khẩu nhập lại không đúng với mật khẩu');
        }
    }



    // Xem chi tiết sân còn trống ngày hôm nay
    public function view_detail_football_ground($ma_san_bong)
    {
        // hiển thị các khung giờ đặt lịch còn trống
        // khi chọn khung giờ điều hướng đến trang tạo hóa đơn
        $class_body     = "section-white";
        
        $san_bong       = new san_bong_model();
        $array_san_bong = $san_bong->check_ma_san_bong();
        
        $check_san_bong = FALSE;
        
        foreach ($array_san_bong as $row) {
            if ($row->ma_san_bong == $ma_san_bong)
            {
                $check_san_bong = TRUE;
            }
        }
        
        if ($check_san_bong == FALSE) {
            return redirect()->route('customer.home')->with('error','Sân bóng không tồn tại');;
        }
        
        // khung giờ có giá trị 1 được chọn, 0 không được chọn
		$san_bong->ma_san_bong = $ma_san_bong;
		$san_bong              = $san_bong->get_one();

        // dd(Helper::loai_san_bong());

        return view('customer.view_detail_football_ground',[
            'class_body'     => $class_body,
            'array_san_bong' => $san_bong,
        ]);
    }

    // Tìm sân theo ngày - tên
    public function view_search_football_ground(validation_form_search $request)
    {
        $class_body                     = "blog-posts";
        
        $khung_gio                      = new khung_gio_dat_lich_model();
        // Khung giờ có giá trị 1 được chọn, 0 không được chọn
        $khung_gio->chon                = 1;
        $array_khung_gio                = $khung_gio->get_all();
        
        
        $ngay_da                        = $request->input('ngay_da');

        // Format date
        $date                           = str_replace('/', '-', $ngay_da);
        $ngay_da                        = date( 'Y/m/d', strtotime($date) );
        


        $san_bong                       = new san_bong_model();
        $san_bong->ma_san_bong          = Request::get('ten_san_bong');
        $array_san_bong                 = $san_bong->get_all();
        


        // Check khung gio dat lich
        $hoa_don                        = new hoa_don_model();
        $hoa_don->ngay_da               = $ngay_da;
        $array_check_khung_gio_dat_lich = $hoa_don->check_khung_gio_dat_lich();
        

        return view('customer.view_search_football_ground',[
            'class_body'                     => $class_body,
            'array_khung_gio'                => $array_khung_gio,
            'check_ma_san_bong'              => Request::get('ten_san_bong'),
            'array_san_bong'                 => $array_san_bong,
            'array_check_khung_gio_dat_lich' => $array_check_khung_gio_dat_lich,
            'ngay_da'                        => $ngay_da
        ]);
    }

    // Đặt lịch thuê sân
    public function view_book_football_ground(validation_form_date $request, $ma_san_bong)
    {
        $class_body               = "profile-page";

        $san_bong       = new san_bong_model();
        $array_san_bong = $san_bong->check_ma_san_bong();

        $check_san_bong = FALSE;
        foreach ($array_san_bong as $row) {
            if ($row->ma_san_bong == $ma_san_bong)
            {
                $check_san_bong = TRUE;
            }
        }
        
        if ($check_san_bong == FALSE) {
            return redirect()->route('customer.home')->with('error','Sân bóng không tồn tại');;
        }
        
        // Khung giờ có giá trị 1 được chọn, 0 không được chọn
        $khung_gio_dat_lich       = new khung_gio_dat_lich_model();
        $khung_gio_dat_lich->chon = 1;
        $khung_gio                = $khung_gio_dat_lich->get_all();


        // Lấy thông tin đẩy lên và tạo session
        // Tránh khi chưa đăng nhập
        $ngay_thue    = $request->input('result_date');
        $ma_khung_gio = $request->input('result_time');


        // Tạo session('ma_san_bong'), session('ngay_thue') và session('ma_khung_gio')
        if (!empty($ngay_thue) && !empty($ma_khung_gio))
        {
            Session::put('ma_san_bong',$ma_san_bong);
            Session::put('ngay_thue',$ngay_thue);
            Session::put('ma_khung_gio',$ma_khung_gio);
        }

        // Nếu đủ session thì điều hướng sang trang xác nhận thông tin người tạo
        if ( !empty(Session::get('ma_san_bong')) && !empty(Session::get('ngay_thue')) && !empty(Session::get('ma_khung_gio')) )
        {
            return redirect()->route('customer.view_create_bill_once_football_ground');
        }


        // Ngày được chọn
        if (!empty(Request::get('date')))
        {
            $date = Request::get('date');
            $date = str_replace('/', '-', $date);
            $date = date('Y/m/d', strtotime($date) );
        }
        else
        {
           // Ngày đá mặc định
            $date = date('Y/m/d');
            $date = date('Y/m/d', strtotime($date.'+1 day') ); 
        }
        


        // Check khung gio dat lich
        $hoa_don                        = new hoa_don_model();
        $hoa_don->ngay_da               = $date;
        $array_check_khung_gio_dat_lich = $hoa_don->check_khung_gio_dat_lich();


        // Định dạng lại ngày đá cho khách hàng dễ nhìn
        if (!empty(Request::get('date')))
        {
            $date = Request::get('date');
        }
        else
        {
            $date = date('Y/m/d');
            $date = date('d/m/Y', strtotime($date.'+1 days') );
        }



        return view('customer.view_book_football_ground',[
            'class_body'                     => $class_body,
            'array_khung_gio'                => $khung_gio,
            'date'                           => $date,
            'ma_san_bong'                    => $ma_san_bong,
            'array_check_khung_gio_dat_lich' => $array_check_khung_gio_dat_lich
        ]);
    }

    public function ajax_check_book_football_ground(Request $request)
    {
        $id   = $_GET['id'];
        $date = $_GET['date'];
        $date = str_replace('/', '-', $date);
        $date = date('Y/m/d', strtotime($date) );

        // Check khung gio dat lich
        $hoa_don                        = new hoa_don_model();
        $hoa_don->ngay_da               = $date;
        $array_check_khung_gio_dat_lich = $hoa_don->check_khung_gio_dat_lich();

        $data = json_encode($array_check_khung_gio_dat_lich);

        return $data;
    }

    public function view_book_hour_football_ground(Request $request)
    {
        $class_body  = "profile-page";


        $ngay_da     = Request::get('ngay_da');
        $ma_san_bong = Request::get('ma_san_bong');

        if( !empty($ngay_da) && !empty($ma_san_bong) )
        {
            if(!empty( Session::get('ma_san_bong') ) && !empty( Session::get('ngay_thue') ) && !empty( Session::get('ma_khung_gio') ))
            {
                Session::forget(['ma_san_bong', 'ngay_thue', 'ma_khung_gio']);
            }
        }

        // Tạo session('ma_san_bong'), session('ngay_thue') và session('ma_khung_gio')
        if(!empty($ngay_da) && !empty($ma_san_bong) && !empty( Request::get('khung_gio') ))
        {
            $ma_khung_gio = Request::get('khung_gio');

            Session::put('ma_san_bong',$ma_san_bong);
            Session::put('ngay_thue',$ngay_da);
            Session::put('ma_khung_gio',$ma_khung_gio);
        }

        // Nếu đủ session thì điều hướng sang trang xác nhận thông tin người tạo
        if (!empty( Session::get('ma_san_bong') ) && !empty( Session::get('ngay_thue') ) && !empty( Session::get('ma_khung_gio') ))
        {
            return redirect()->route('customer.view_create_bill_once_football_ground');
        }


        // Xét khung giờ hiển thị
        // TH1: ngày trong quá khứ, click vào sẽ về trang chủ và thông báo lỗi
        // TH2: ngày hiện tại thì chỉ hiển thị khung giờ lớn hơn giờ hiện tại và hiện thị khung giờ chưa được đặt
        // TH3: hiển thị những khung giờ chưa được đặt
        date_default_timezone_set('Asia/Bangkok');
        $ngay_hien_tai = date('Y/m/d');
        $gio_hien_tai  = date('H:i:s');

        if($ngay_hien_tai >= $ngay_da)
        {
            return redirect()->back()->with('error','Bạn phải chọn ngày đá lớn hơn ngày hiện tại');
        }


        // Khung giờ có giá trị 1 được chọn, 0 không được chọn
        $khung_gio_dat_lich       = new khung_gio_dat_lich_model();
        $khung_gio_dat_lich->chon = 1;
        $khung_gio                = $khung_gio_dat_lich->get_all();

        // Check khung gio dat lich
        $hoa_don                        = new hoa_don_model();
        $hoa_don->ngay_da               = $ngay_da;
        $array_check_khung_gio_dat_lich = $hoa_don->check_khung_gio_dat_lich();


        return view('customer.view_book_hour_football_ground',[
            'class_body'                     => $class_body,
            'ma_san_bong'                    => $ma_san_bong,
            'array_khung_gio'                => $khung_gio,
            'array_check_khung_gio_dat_lich' => $array_check_khung_gio_dat_lich,
            'ngay_da'                        => $ngay_da
        ]);
    }

    // Đặt giải trong nhiều ngày
    public function view_book_tournament(validation_form_date_tournament $request)
    {
        $class_body      = "profile-page";
        
        $khung_gio       = new khung_gio_dat_lich_model();
        
        // Khung giờ có giá trị 1 được chọn, 0 không được chọn
        $khung_gio->chon = 1;
        $array_khung_gio = $khung_gio->get_all();

        $ngay_bat_dau  = date('d/m/Y', strtotime("+1 days"));
        $ngay_ket_thuc = date('d/m/Y', strtotime("+2 days"));


        if( !empty($request->input('date_start')) && !empty($request->input('date_end')) && !empty($request->input('time')) )
        {
            $date_start = $request->input('date_start');
            $date_end   = $request->input('date_end');
            $time       = $request->input('time');

            if( is_numeric($time) )
            {
                // Lấy khung giờ
                $khung_gio->ma_khung_gio = $time;
                $time                    = $khung_gio->get_khung_gio();


                // covert date start
                $date_start = str_replace('/', '-', $date_start);
                $date_start = date( 'Y-m-d',strtotime($date_start) );
                
                // covert date end
                $date_end   = str_replace('/', '-', $date_end);
                $date_end   = date( 'Y-m-d',strtotime($date_end) );
                
                $date1      = date_create($date_start);
                $date2      = date_create($date_end);
                $diff       = date_diff($date2,$date1);
                
                $array_date         = array();
                $array_data_default = array();

                // get day diff
                $day_diff = $diff->d;

                for ($i=0; $i <= $day_diff; $i++)
                {
                    $data_default = date('Y/m/d',strtotime($date_start . "+$i days"));
                    array_push($array_data_default,$data_default);

                    // convert date d/m/Y
                    $date = date('d/m/Y',strtotime($date_start . "+$i days"));
                    array_push($array_date,$date);
                }

                if( !empty($array_date) && !empty($time) )
                {
                    $count_date = count($array_date);

                    return view('customer.view_check_football_ground_tournament',[
                        'class_body'         => $class_body,
                        'array_date'         => $array_date,
                        'array_data_default' => $array_data_default,
                        'time'               => $time,
                        // Tổng số ngày đá
                        'tong_so'            => $count_date,
                    ]);
                }
            }
            else
            {
                return redirect()->route('customer.view_book_tournament')->with('error','Khung giờ đá bị sai, bạn hãy chọn lại');
            }
        }

        // Check khung gio dat lich
        // $hoa_don                        = new hoa_don_model();
        // $hoa_don->ngay_da               = $ngay_da;
        // $array_check_khung_gio_dat_lich = $hoa_don->check_khung_gio_dat_lich();

        return view('customer.view_book_tournament',[
            'class_body'                        => $class_body,
            'array_khung_gio'                   => $array_khung_gio,
            // 'array_check_khung_gio_dat_lich' => $array_check_khung_gio_dat_lich
            'ngay_bat_dau'                      => $ngay_bat_dau,
            'ngay_ket_thuc'                     => $ngay_ket_thuc
        ]);
    }

    // Đặt giải trong 1 ngày - Chọn ngày đá giải
    public function view_select_date_tournament(validation_form_date $request)
    {
        $class_body = "profile-page";

        $get_value_submit = $request->all();

        if( !empty($get_value_submit['khung_gio']) && count($get_value_submit['khung_gio']) < 2 )
        {
            return redirect()->route('customer.view_select_date_tournament')->with('error','Ngày đá phải phải chọn nhiều hơn 2');
        }

        if( !empty($get_value_submit) )
        {
            $date  = $get_value_submit['result_date'];
            $times = collect($get_value_submit['khung_gio']);
            Session::put('date', $date);
            Session::put('times', $times);
            if( !empty(Session::get('date')) &&  !empty(Session::get('times')) )
            {
                return redirect()->route('customer.view_check_football_ground_tournament');
            }
        }

        date_default_timezone_set('Asia/Bangkok');
        $ngay_da = date('Y/m/d');
        $ngay_da = date('d/m/Y', strtotime($ngay_da.'+1 day') ); 

        $khung_gio       = new khung_gio_dat_lich_model();
        
        // Khung giờ có giá trị 1 được chọn, 0 không được chọn
        $khung_gio->chon = 1;
        $array_khung_gio = $khung_gio->get_all();

        // Check khung gio dat lich
        $hoa_don                        = new hoa_don_model();
        $hoa_don->ngay_da               = $ngay_da;
        $array_check_khung_gio_dat_lich = $hoa_don->check_khung_gio_dat_lich();

        return view('customer.view_select_date_tournament',[
            'class_body'                     => $class_body,
            'ngay_da'                        => $ngay_da,
            'array_khung_gio'                => $array_khung_gio,
            'array_check_khung_gio_dat_lich' => $array_check_khung_gio_dat_lich
        ]);
    }

    // Chọn sân sau khi đặt giải
    public function view_check_football_ground_tournament()
    {
        $class_body = "profile-page";

        $ngay_da         = Session::get('date');
        $nhieu_khung_gio = Session::get('times');

        // Check hóa đơn 'ngay_da' và 'nhieu_khung_gio' -> lấy ra mã sân bóng(tiền sân) còn trống
        $hoa_don_chi_tiet          = new hoa_don_chi_tiet_model();
        // Convert date
        $check_ngay_da             = str_replace('/', '-', $ngay_da);
        $check_ngay_da             = date( 'Y-m-d',strtotime($check_ngay_da) );
        $hoa_don_chi_tiet->ngay_da = $check_ngay_da;
        $check_hoa_don             = $hoa_don_chi_tiet->get_bill_for_date();


        $khung_gio       = new khung_gio_dat_lich_model();
        $tong_so         = count(Session::get('times'));
        
        $array_khung_gio = array();

        for ($i=0; $i < $tong_so; $i++)
        {
            // Check khung giờ đã được lấy theo ngày đá
            foreach ($check_hoa_don as $row_check)
            {
                // Nếu mã khung giờ tồn tại trong đơn thì loại bỏ mã sân bóng đã có trong hóa đơn
                if($row_check->ma_khung_gio == $nhieu_khung_gio[$i])
                {
                    // $row_check->ma_san_bong = 0;
                }
            }
            // Lấy khung giờ
            $khung_gio->ma_khung_gio = $nhieu_khung_gio[$i];
            $khung_gio_dat_lich      = $khung_gio->get_khung_gio();
            array_push($array_khung_gio, $khung_gio_dat_lich->khung_gio);
        }

        // dd($check_hoa_don);

        return view('customer.view_check_football_ground_tournament',[
            'class_body'      => $class_body,
            'ngay_da'         => $ngay_da,
            'nhieu_khung_gio' => $nhieu_khung_gio,
            'array_khung_gio' => $array_khung_gio,
            'tong_so'         => $tong_so,
            'check_hoa_don'   => $check_hoa_don,
        ]);
    }

    // Chọn sân(hiện chưa dùng)
	public function view_football_ground()
    {
        $ngay_da        = Request::get('result_date');
        $ma_khung_gio   = Request::get('result_time');
        
        $san_bong       = new san_bong_model();
        $array_san_bong = $san_bong->get_all();

    	return view('customer.view_football_ground',[
            'array_san_bong' => $array_san_bong,
            'ngay_da'        => $ngay_da,
            'ma_khung_gio'   => $ma_khung_gio
        ]);
    }



    // Tạo session để tạo hóa đơn
    // Tạo session('ma_san_bong'), session('ngay_thue') và session('ma_khung_gio')
    public function create_session_to_create_bill()
    {
        // Lấy thông tin đẩy lên và tạo session
        // Tránh khi chưa đăng nhập
        $ngay_thue    = Request::get('result_date');
        $ma_khung_gio = Request::get('result_time');
        $ma_san_bong  = Request::get('result_football_ground');


        // Tạo session('ma_san_bong'), session('ngay_thue') và session('ma_khung_gio')
        if ( !empty($ngay_thue) && !empty($ma_khung_gio) && !empty($ma_san_bong) )
        {
            Session::put('ma_san_bong',$ma_san_bong);
            Session::put('ngay_thue',$ngay_thue);
            Session::put('ma_khung_gio',$ma_khung_gio);

            return redirect()->route('customer.view_create_bill_once_football_ground');
        }
        else
        {
            return redirect()->route('customer.view_search_football_ground')->with('error','Xảy ra lỗi trong quá trình đặt sân');
        }
    }

    // Tạo hóa đơn thuê sân
    public function view_create_bill_once_football_ground()
    {
        $class_body                = "section-white";
        
        $khach_hang                = new khach_hang_model();
        $khach_hang->ma_khach_hang = Session::get('ma_khach_hang');
        $array_khach_hang          = $khach_hang->get_detail_once();

        return view('customer.view_create_bill_once_football_ground',[
            'class_body'       => $class_body,
            'array_khach_hang' => $array_khach_hang
        ]);
    }

    // Xử lý tạo hóa đơn thuê sân
    // Xóa session('ma_san_bong'), session('ngay_thue') và session('ma_khung_gio')
    public function create_bill_once_football_ground(validation_form_create_bill_once_football_ground $request)
    {
        $ten_khach_hang   = $request->input('ten_khach_hang');
        $so_dien_thoai    = $request->input('so_dien_thoai');
        $email_khach_hang = $request->input('email_khach_hang');
        
        // Format date
        $date             = str_replace('/', '-', Session::get('ngay_thue'));
        $ngay_thue        = date( 'Y/m/d', strtotime($date) );

        // Check ma_khung_gio đã được đặt chưa
        // Nếu được đặt rồi thì xóa session và thông báo lỗi
        $hoa_don                        = new hoa_don_model();
        $hoa_don_chi_tiet               = new hoa_don_chi_tiet_model();
        $hoa_don->ngay_da               = $ngay_thue;
        $array_check_khung_gio_dat_lich = $hoa_don->check_khung_gio_dat_lich();

        foreach ($array_check_khung_gio_dat_lich as $row_check_khung_gio_dat_lich) {
            if ($row_check_khung_gio_dat_lich->ma_khung_gio == Session::get('ma_khung_gio') && $row_check_khung_gio_dat_lich->ma_san_bong == Session::get('ma_san_bong')) {

                // Xóa session
                Session::forget(['ma_san_bong', 'ngay_thue', 'ma_khung_gio']);

                return redirect()->route('customer.view_bill')->with('error','Khung giờ đã được đặt, bạn hãy chọn khung giờ khác');
            }
            // Nếu không có lỗi thì tiếp tục tạo hóa đơn
        }
        
        
        
        // Lấy tiền thuê sân để tính thành tiền hóa đơn
        $san_bong               = new san_bong_model();
        $san_bong->ma_san_bong  = Session::get('ma_san_bong');
        $array_san_bong         = $san_bong->get_one();
        
        
        // Insert vào bảng hoa_don
        // Tiền đặt cọc sắp thêm
        $hoa_don->tien_dat_coc  = 0;
        $hoa_don->thanh_tien    = $array_san_bong->gia;
        $hoa_don->tinh_trang    = -2;
        
        date_default_timezone_set('Asia/Bangkok');
        $hoa_don->ngay_tao      = date('Y-m-d H:i:s');
        
        $hoa_don->ma_khach_hang = Session::get('ma_khach_hang');
        
        $ma_hoa_don             = $hoa_don->insert();
        

        // Insert vào bảng chi_tiet_hoa_don
        $hoa_don_chi_tiet->ma_hoa_don   = $ma_hoa_don;
        $hoa_don_chi_tiet->ma_san_bong  = Session::get('ma_san_bong');
        $hoa_don_chi_tiet->ma_khung_gio = Session::get('ma_khung_gio');
        $hoa_don_chi_tiet->ngay_da      = $ngay_thue;
        $hoa_don_chi_tiet->gia          = $array_san_bong->gia;
        $hoa_don_chi_tiet->insert();
         

        // Kiểm tra nếu 3 session('ma_san_bong'), session('ngay_thue') và session('ma_khung_gio')
        if( Session::has('ma_san_bong') && Session::has('ngay_thue') && Session::has('ma_khung_gio') )
        {
            // Xóa session và thông báo thêm thành công
            Session::forget(['ma_san_bong', 'ngay_thue', 'ma_khung_gio']);
            return redirect()->route('customer.view_bill')->with('success','Đặt sân thành công');
        }
        else
        {
            return redirect()->route('customer.view_bill')->with('error','Xảy ra lỗi trong quá trình đặt sân');
        }
    }

    // Xem hóa đơn thuê sân
    public function view_bill()
    {
        $class_body             = "";
        
        $ma_khach_hang          = Session::get('ma_khach_hang');
        
        $hoa_don                = new hoa_don_model();
        
        $hoa_don->ma_khach_hang = $ma_khach_hang;
        $hoa_don                = $hoa_don->get_info_bill();

        // dd($hoa_don);

        return view('customer.view_bill',[
            'class_body' => $class_body,
            'hoa_don'    => $hoa_don
        ]);
    }



    // Lịch thuê sân
    public function calendar_football_ground()
    {
        $class_body  = "";
        
        $ma_san_bong = Request::get('ma_san_bong');

        return view('customer.view_calendar_football_ground',[
            'class_body'  => $class_body,
            'ma_san_bong' => $ma_san_bong
        ]);
    }

    // Tải lịch thuê sân - lấy từ hóa đơn
    public function load_calendar()
    {
        $hoa_don = new hoa_don_model();
        
        $hoa_don = $hoa_don->load_calendar_for_bill();
        
        $array   = array();

        foreach ($hoa_don as $row) {

            $gio_bat_dau  = $row->ngay_da.'T'.$row->gio_bat_dau;
            $gio_ket_thuc = $row->ngay_da.'T'.$row->gio_ket_thuc;

            if ($row->tinh_trang == 1) {
                $title = "Đã thanh toán";
                $color = "green";
            }
            elseif ($row->tinh_trang == 0) {
                $title = "Chưa thanh toán";
                $color = "orange";
            }
            elseif ($row->tinh_trang == -1) {
                $title = "Đã đặt cọc";
                $color = "brown";
            }
            elseif ($row->tinh_trang == -2) {
                $title = "Chưa đặt cọc";
                $color = "red";
            }

            $title = "Sân số ".$row->ma_san_bong.", ".$row->khung_gio.", ".$title;
            
            $data  = array(
                "id"        => $row->ma_hoa_don,
                "title"     => $title,
                "start"     => $gio_bat_dau,  // datetime start
                "end"       => $gio_ket_thuc, // datetime end
                "color"     => $color, 
                "textColor" => "white",
                "className" => "rounded",
            );
            array_push($array, $data);
        }

        return json_encode($array);
    }

    // Xem lịch thuê của mỗi sân bóng
    public function view_calendar_detail_football_ground()
    {
        $class_body  = "";
        $ma_san_bong = Request::get('ma_san_bong');

        // Kiểm tra xem có tồn tại ma_san_bong
        $san_bong              = new san_bong_model();
        $san_bong->ma_san_bong = $ma_san_bong;
        $san_bong              = $san_bong->check_ma_san_bong();
        
        $check_ma_san_bong     = TRUE;

        foreach ($san_bong as $row) {
            if($row->ma_san_bong == $ma_san_bong)
            {
                $check_ma_san_bong = TRUE;
                break;
            }
            else
            {
                $check_ma_san_bong = FALSE;
            }
        }

        // Nếu mã sân bóng không đúng thì quay lại trang chủ và báo lỗi
        if($check_ma_san_bong == TRUE)
        {
            return view('customer.view_calendar_football_ground',[
                'class_body'  => $class_body,
                'ma_san_bong' => $ma_san_bong,
            ]);
        }
        else
        {
            return redirect()->route('customer.home')->with('error','Sân bóng không tồn tại');
        }
        
    }

    // Tải lịch thuê của mỗi sân bóng
    public function load_calendar_detail_football_ground($ma_san_bong)
    {
        $hoa_don              = new hoa_don_model();
        $hoa_don->ma_san_bong = $ma_san_bong;
        $hoa_don              = $hoa_don->load_calendar_detail_football_ground_for_bill();
        
        $array                = array();

        foreach ($hoa_don as $row) {

            $gio_bat_dau  = $row->ngay_da.'T'.$row->gio_bat_dau;
            $gio_ket_thuc = $row->ngay_da.'T'.$row->gio_ket_thuc;

            if ($row->tinh_trang == 1) {
                $title = "Đã thanh toán";
                $color = "green";
            }
            elseif ($row->tinh_trang == 0) {
                $title = "Chưa thanh toán";
                $color = "orange";
            }
            elseif ($row->tinh_trang == -1) {
                $title = "Đã đặt cọc";
                $color = "brown";
            }
            elseif ($row->tinh_trang == -2) {
                $title = "Chưa đặt cọc";
                $color = "red";
            }

            $title = $row->khung_gio." ".$title;
            
            $data  = array(
                "id"        => $row->ma_hoa_don,
                "title"     => $title,
                "start"     => $gio_bat_dau,  // datetime start
                "end"       => $gio_ket_thuc, // datetime end
                "color"     => $color, 
                "textColor" => "white",
                "className" => "rounded",
            );
            array_push($array, $data);
        }

        return json_encode($array);
    }



    // Quản lý profile
	public function view_profile()
    {
        $class_body                = "";

        $khach_hang                = new khach_hang_model();
        $khach_hang->ma_khach_hang = Session::get('ma_khach_hang');
        $array_khach_hang          = $khach_hang->get_detail_once();
    	return view('customer.view_profile',[
            'class_body'       => $class_body,
            'array_khach_hang' => $array_khach_hang[0]
        ]);
    }

    // Update profile
    public function update_profile(validation_form_register $request)
    {
        $khach_hang                   = new khach_hang_model();
        $khach_hang->ma_khach_hang    = Session::get('ma_khach_hang');
        $khach_hang->so_dien_thoai    = Request::get('so_dien_thoai');
        $khach_hang->email_khach_hang = Request::get('email_khach_hang');
        $array_khach_hang             = $khach_hang->update_profile();

        return redirect()->route('customer.view_profile')->with('success','Thay đổi thông tin thành công');
    }
}