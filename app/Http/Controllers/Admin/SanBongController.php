<?php

namespace App\Http\Controllers\Admin;

use Request;
use Storage;
use App\Models\Admin\SanBong;

class SanBongController extends Controller
{
	public function view_all()
	{
		$san_bong       = new SanBong();
        $array_san_bong = $san_bong->get_all();
        return view('admin.san_bong.view_all',[
            'array_san_bong' => $array_san_bong
        ]);
	}

	public function view_insert()
	{
		return view('admin.san_bong.view_insert');
	}

	public function process_insert()
	{
		// Copy file ảnh tới thư mục storage/app/public
		$anh  = Request::file('anh');
		$path = Storage::disk('public')->put('',$anh);

		$san_bong                = new SanBong();
		$san_bong->loai_san_bong = Request::get('loai_san_bong');
		$san_bong->gia           = Request::get('gia');
		$san_bong->anh           = $path;

		$san_bong->insert();

        return redirect()->route('san_bong.view_all');
	}

	public function view_update($id)
	{
		$san_bong              = new SanBong();
		$san_bong->ma_san_bong = $id;
		$san_bong              = $san_bong->get_one();
		return view('admin.san_bong.view_update',[
			'san_bong' => $san_bong
		]);
	}

	public function process_update($id)
	{
		// Format money
		$gia = Request::get('gia');
		$gia = str_replace(',', '', $gia);

		$san_bong              = new SanBong();
		$san_bong->ma_san_bong = $id;
		$san_bong->loai_san_bong = Request::get('loai_san_bong');
		$san_bong->gia           = $gia;
 	 	$san_bong->update();

 	 	return redirect()->route('san_bong.view_all');
	}

	// public function process_delete($id)
 	// {
	// 	$san_bong           = new san_bong();
	// 	$san_bong->ma_san_bong = $id;
	// 	$san_bong->delete();

 	//  return redirect()->route('admin.san_bong.view_all');
 	//  }
}