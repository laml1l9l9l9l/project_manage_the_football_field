<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Models\Admin\HoaDon;

Class HoaDonController extends Controller
{
	public function view_all()
	{
		$hoa_don       = new HoaDon();
        $array_hoa_don = $hoa_don->get_all();
        return view('admin.hoa_don.view_all',[
            'array_hoa_don' => $array_hoa_don
        ]);
	}

}