<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Models\Admin\Admin;

class AdminController extends Controller
{
	public function view_all()
	{
		$admin       = new Admin();
        $array_admin = $admin->get_all();
        return view('admin.admin.view_all',[
            'array_admin' => $array_admin
        ]);
	}

	public function view_insert()
	{
		return view('admin.admin.view_insert');
	}

	public function process_insert()
	{
		$admin                  = new Admin();
		$admin->mat_khau        = Request::get('mat_khau');
		$admin->ten_admin       = Request::get('ten_admin');
		$admin->email_admin     = Request::get('email_admin');
		$admin->so_dien_thoai   = Request::get('so_dien_thoai');
		$admin->phan_quyen      = Request::get('phan_quyen');
        $admin->insert();

        return redirect()->route('admin.view_all');
	}

	public function view_update($id)
	{
		$admin           = new Admin();
		$admin->ma_admin = $id;
		$admin           = $admin->get_one();
		return view('admin.admin.view_update',[
			'admin' => $admin
		]);
	}

	public function process_update($id)
	{
		$admin                  = new Admin();
		$admin->ma_admin        = $id;
		$admin->mat_khau        = Request::get('mat_khau');
		$admin->ten_admin       = Request::get('ten_admin');
		$admin->email_admin     = Request::get('email_admin');
		$admin->so_dien_thoai   = Request::get('so_dien_thoai');
		$admin->phan_quyen      = Request::get('phan_quyen');
        $admin->update();

        return redirect()->route('admin.view_all');
	}

	public function process_delete($id)
    {
		$admin           = new Admin();
		$admin->ma_admin = $id;
		$admin->delete();

        return redirect()->route('admin.view_all');
    }
}