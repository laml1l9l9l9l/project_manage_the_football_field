<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Models\Admin\Admin;
use Request;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function layer()
    {
    	return view('layer.master');
    }
    public function view_login()
    {
        if( !empty(Session::get('ma_admin')) && !empty(Session::get('ten_admin')) )
        {
            return redirect()->route('wellcome');
        }
    	return view('admin.view_login');
    }
    public function process_login()
    {
        $admin              = new Admin();
        $admin->email_admin = Request::get('email_admin');
        $admin->mat_khau    = Request::get('mat_khau');    	
        $admin              = $admin->login();

    	if(count($admin) == 1){
    		Session::put('ma_admin', $admin[0]->ma_admin);
    		Session::put('ten_admin', $admin[0]->ten_admin);
            // Session::put('anh_admin', $admin[0]->anh_admin);

    		return redirect()->route('wellcome');
    	}
    	return redirect()->route('admin.view_login')->with('error','Đăng nhập thất bại');
    }
    public function logout()
    {
        Session::flush();
        return redirect()->route('admin.view_login')->with('success','Đăng xuất thành công');
    }
    public function wellcome()
    {
    	return view('admin.wellcome');
    }
    public function calendar()
    {
        return view('admin.Calendar');
    }
}