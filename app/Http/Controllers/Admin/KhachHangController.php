<?php

namespace App\Http\Controllers\Admin;

use Request;
use App\Models\Admin\KhachHang;

class KhachHangController extends Controller
{
	public function view_all()
	{
		$khach_hang       = new KhachHang();
        $array_khach_hang = $khach_hang->get_all();
        return view('admin.khach_hang.view_all',[
            'array_khach_hang' => $array_khach_hang
        ]);
	}
}	