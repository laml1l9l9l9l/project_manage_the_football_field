<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class check_profile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('ma_khach_hang')) {
            return $next($request);
        }
        return redirect()->route('customer.view_login')->with('error','Bạn phải đăng nhập');
    }
}
