<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NhaPhanPhoi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nha_phan_phoi', function (Blueprint $table) {
            $table->increments('ma_nha_phan_phoi');
            $table->string('ten_nha_phan_phoi',50);
            $table->string('so_dien_thoai_nha_phan_phoi',15)->unique();
            $table->string('email_nha_phan_phoi',100)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nha_phan_phoi');
    }
}
