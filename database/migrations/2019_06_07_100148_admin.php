<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Admin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->increments('ma_admin');
            $table->string('tai_khoan_admin',50)->unique();
            $table->string('mat_khau',50);
            $table->string('ten_admin',50);
            $table->string('email_admin',100)->unique();
            $table->string('so_dien_thoai',15)->unique();
            $table->tinyInteger('phan_quyen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
