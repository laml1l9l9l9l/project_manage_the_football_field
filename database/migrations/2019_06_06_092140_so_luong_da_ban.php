<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoLuongDaBan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('so_luong_da_ban', function (Blueprint $table) {
            $table->date('ngay');
            $table->integer('ma_san_pham');
            $table->integer('so_luong');
            $table->primary(['ngay','ma_san_pham']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('so_luong_da_ban');
    }
}
