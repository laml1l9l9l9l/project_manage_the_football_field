<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KhungGioDatLich extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('khung_gio_dat_lich', function (Blueprint $table) {
            $table->increments('ma_khung_gio');
            $table->time('gio_bat_dau');
            $table->time('gio_ket_thuc');
            $table->string('khung_gio',50);
            $table->float('cap_so_nhan', 8, 0);
            $table->tinyInteger('chon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('khung_gio_dat_lich');
    }
}
