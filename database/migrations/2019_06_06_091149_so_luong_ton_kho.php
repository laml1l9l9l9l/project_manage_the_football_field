<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SoLuongTonKho extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('so_luong_ton_kho', function (Blueprint $table) {
            $table->integer('thang');
            $table->integer('nam');
            $table->integer('ma_san_pham');
            $table->integer('so_luong');
            $table->primary(['thang','nam','ma_san_pham']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('so_luong_ton_kho');
    }
}
