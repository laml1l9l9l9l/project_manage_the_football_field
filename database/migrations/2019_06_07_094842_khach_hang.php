<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KhachHang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('khach_hang', function (Blueprint $table) {
            $table->increments('ma_khach_hang');
            $table->string('tai_khoan_khach_hang',50)->unique();
            $table->string('mat_khau',50);
            $table->string('ten_khach_hang',50);
            $table->string('email_khach_hang',100)->unique();
            $table->string('so_dien_thoai',15)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('khach_hang');
    }
}
