<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChiTietHoaDon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chi_tiet_hoa_don', function (Blueprint $table) {
            $table->integer('ma_hoa_don');
            $table->integer('ma_san_bong');
            $table->integer('ma_khung_gio');
            $table->primary(array('ma_hoa_don', 'ma_san_bong', 'ma_khung_gio'));
            $table->date('ngay_da');
            $table->float('gia', 8, 0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chi_tiet_hoa_don');
    }
}
