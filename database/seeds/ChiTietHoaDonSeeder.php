<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ChiTietHoaDonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('chi_tiet_hoa_don')->insert([
            [
				'ma_hoa_don'   => '2',
				'ma_san_bong'  => '1',
				'ma_khung_gio' => '9',
				'ngay_da'      => '2019-07-26',
				'gia'          => '1000000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'   => '13',
				'ma_san_bong'  => '2',
				'ma_khung_gio' => '12',
				'ngay_da'      => '2019-08-04',
				'gia'          => '700000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'   => '14',
				'ma_san_bong'  => '2',
				'ma_khung_gio' => '5',
				'ngay_da'      => '2019-08-05',
				'gia'          => '700000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'   => '15',
				'ma_san_bong'  => '1',
				'ma_khung_gio' => '1',
				'ngay_da'      => '2019-08-09',
				'gia'          => '1000000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'   => '16',
				'ma_san_bong'  => '1',
				'ma_khung_gio' => '1',
				'ngay_da'      => '2019-08-11',
				'gia'          => '1000000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'   => '17',
				'ma_san_bong'  => '1',
				'ma_khung_gio' => '2',
				'ngay_da'      => '2019-08-12',
				'gia'          => '1000000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'   => '18',
				'ma_san_bong'  => '2',
				'ma_khung_gio' => '1',
				'ngay_da'      => '2019-08-13',
				'gia'          => '700000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'   => '19',
				'ma_san_bong'  => '2',
				'ma_khung_gio' => '8',
				'ngay_da'      => '2019-08-15',
				'gia'          => '700000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'   => '20',
				'ma_san_bong'  => '1',
				'ma_khung_gio' => '7',
				'ngay_da'      => '2019-08-16',
				'gia'          => '1000000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'   => '21',
				'ma_san_bong'  => '1',
				'ma_khung_gio' => '12',
				'ngay_da'      => '2019-08-16',
				'gia'          => '1000000',
				'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);
    }
}
