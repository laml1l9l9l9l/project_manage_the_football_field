<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class NgaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ngay')->insert([
            [
                'ma_ngay'     => '1',
                'ngay'        => '2019-07-26',
                'dac_biet'    => '0',
                'cap_so_nhan' => '0',
                'nghi'        => '0',
                'created_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);
    }
}
