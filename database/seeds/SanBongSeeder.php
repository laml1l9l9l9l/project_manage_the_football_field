<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SanBongSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('san_bong')->insert([
            [
                'ma_san_bong'   => '1',
                'loai_san_bong' => '2',
                'gia'           => '1000000',
                'anh'           => 'football_ground_1.jpg',
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_san_bong'   => '2',
                'loai_san_bong' => '1',
                'gia'           => '700000',
                'anh'           => 'football_ground_2.jpg',
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);
    }
}
