<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(HoaDonSeeder::class);
        $this->call(KhachHangSeeder::class);
        $this->call(KhungGioDatLichSeeder::class);
        $this->call(NgaySeeder::class);
        $this->call(SanBongSeeder::class);
        $this->call(ChiTietHoaDonSeeder::class);
    }
}
