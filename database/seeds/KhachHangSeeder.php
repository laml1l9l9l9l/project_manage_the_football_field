<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KhachHangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('khach_hang')->insert([
            [
                'ma_khach_hang'        => '1',
                'tai_khoan_khach_hang' => 'laml1l9l9l9l',
                'mat_khau'             => 'e10adc3949ba59abbe56e057f20f883e',
                'ten_khach_hang'       => 'Nguyễn Tùng Lâm',
                'email_khach_hang'     => 'laml1l9l9l9l@gmail.com',
                'so_dien_thoai'        => '0367718307',
                'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khach_hang'        => '2',
                'tai_khoan_khach_hang' => 'laml9l9l9l9l',
                'mat_khau'             => 'e10adc3949ba59abbe56e057f20f883e',
                'ten_khach_hang'       => 'Lâm Test',
                'email_khach_hang'     => 'laml9l9l9l9l@gmail.com',
                'so_dien_thoai'        => '0123456789',
                'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khach_hang'        => '4',
                'tai_khoan_khach_hang' => 'laml2l2l1l2l',
                'mat_khau'             => 'e10adc3949ba59abbe56e057f20f883e',
                'ten_khach_hang'       => 'Lâm Test 1',
                'email_khach_hang'     => 'laml2l2l1l2l@gmail.com',
                'so_dien_thoai'        => '0987654321',
                'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khach_hang'        => '5',
                'tai_khoan_khach_hang' => 'lamgiaodau_99',
                'mat_khau'             => 'e10adc3949ba59abbe56e057f20f883e',
                'ten_khach_hang'       => 'Nguyễn Lâm',
                'email_khach_hang'     => 'lamgiaodau_99@google.com',
                'so_dien_thoai'        => '0123789456',
                'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khach_hang'        => '6',
                'tai_khoan_khach_hang' => 'lamgiaodau_1999',
                'mat_khau'             => 'e10adc3949ba59abbe56e057f20f883e',
                'ten_khach_hang'       => 'Lâm Nguyễn',
                'email_khach_hang'     => 'lamgiaodau_1999@facebook.com',
                'so_dien_thoai'        => '0456123789',
                'created_at'           => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'           => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);
    }
}
