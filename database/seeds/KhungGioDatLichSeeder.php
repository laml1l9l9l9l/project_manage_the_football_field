<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KhungGioDatLichSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('khung_gio_dat_lich')->insert([
            [
                'ma_khung_gio' => '1',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','05:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','06:00:00')->toDateTimeString(),
                'khung_gio'    => '5am - 6am',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '2',
                'gio_bat_dau'  => '06:00:00',
                'gio_ket_thuc' => '07:00:00',
                'khung_gio'    => '6am - 7am',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '3',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','07:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','08:00:00')->toDateTimeString(),
                'khung_gio'    => '7am - 8am',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '4',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','08:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','09:00:00')->toDateTimeString(),
                'khung_gio'    => '8am - 9am',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '5',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','09:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','10:00:00')->toDateTimeString(),
                'khung_gio'    => '9am - 10am',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '6',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','14:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','15:00:00')->toDateTimeString(),
                'khung_gio'    => '2pm - 3pm',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '7',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','15:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','16:00:00')->toDateTimeString(),
                'khung_gio'    => '3pm - 4pm',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '8',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','16:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','17:00:00')->toDateTimeString(),
                'khung_gio'    => '4pm - 5pm',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '9',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','17:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','18:00:00')->toDateTimeString(),
                'khung_gio'    => '5pm - 6pm',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '10',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','18:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','19:00:00')->toDateTimeString(),
                'khung_gio'    => '6pm - 7pm',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '11',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','19:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','20:00:00')->toDateTimeString(),
                'khung_gio'    => '7pm - 8pm',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'ma_khung_gio' => '12',
                'gio_bat_dau'  => Carbon::createFromFormat('H:i:s','20:00:00')->toDateTimeString(),
                'gio_ket_thuc' => Carbon::createFromFormat('H:i:s','21:00:00')->toDateTimeString(),
                'khung_gio'    => '8pm - 9pm',
                'cap_so_nhan'  => '0',
                'chon'         => '1',
                'created_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'   => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);
    }
}
