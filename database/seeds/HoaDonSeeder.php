<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class HoaDonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hoa_don')->insert([
            [
				'ma_hoa_don'    => '2',
				'tien_dat_coc'  => '100000',
				'thanh_tien'    => '900000',
				'tinh_trang'    => '-1',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-07-25 16:48:17')->toDateTimeString(),
				'ma_khach_hang' => '4',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'    => '13',
				'tien_dat_coc'  => '0',
				'thanh_tien'    => '700000',
				'tinh_trang'    => '1',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-08-04 19:51:40')->toDateTimeString(),
				'ma_khach_hang' => '1',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'    => '14',
				'tien_dat_coc'  => '0',
				'thanh_tien'    => '900000',
				'tinh_trang'    => '0',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-08-05 17:32:28')->toDateTimeString(),
				'ma_khach_hang' => '1',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'    => '15',
				'tien_dat_coc'  => '0',
				'thanh_tien'    => '1000000',
				'tinh_trang'    => '-2',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-08-08 19:27:29')->toDateTimeString(),
				'ma_khach_hang' => '1',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'    => '16',
				'tien_dat_coc'  => '0',
				'thanh_tien'    => '1000000',
				'tinh_trang'    => '-2',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-08-10 16:52:35')->toDateTimeString(),
				'ma_khach_hang' => '1',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'    => '17',
				'tien_dat_coc'  => '0',
				'thanh_tien'    => '900000',
				'tinh_trang'    => '-2',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-08-11 14:52:41')->toDateTimeString(),
				'ma_khach_hang' => '1',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'    => '18',
				'tien_dat_coc'  => '0',
				'thanh_tien'    => '700000',
				'tinh_trang'    => '-2',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-08-12 18:48:32')->toDateTimeString(),
				'ma_khach_hang' => '1',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'    => '19',
				'tien_dat_coc'  => '0',
				'thanh_tien'    => '700000',
				'tinh_trang'    => '-2',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-08-14 18:01:31')->toDateTimeString(),
				'ma_khach_hang' => '1',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'    => '20',
				'tien_dat_coc'  => '0',
				'thanh_tien'    => '1000000',
				'tinh_trang'    => '-2',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-08-15 19:42:25')->toDateTimeString(),
				'ma_khach_hang' => '1',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
				'ma_hoa_don'    => '21',
				'tien_dat_coc'  => '0',
				'thanh_tien'    => '1000000',
				'tinh_trang'    => '-2',
				'ngay_tao'      => Carbon::createFromFormat('Y-m-d H:i:s','2019-08-15 20:05:28')->toDateTimeString(),
				'ma_khach_hang' => '1',
				'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ]);
    }
}
