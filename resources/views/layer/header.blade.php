<nav class="navbar navbar-expand-lg fixed-top navbar-light bg-light rounded py-0">
    <a class="navbar-left" href="{{ route('customer.home') }}">
        <img src="{{ asset('img/logo_transparent.png') }}" alt="Logo" width="80px"
                class="ml-5 mr-0 my-0 px-0 py-0">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbars" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav ml-auto mr-4">
            @if(Session::has('ma_khach_hang'))

                <li class="active nav-item">
                    <a href="{{ route('customer.view_bill') }}" class="nav-link">
                        <i class="material-icons">assignment</i> Hóa đơn
                    </a>
                </li>

                {{-- Quản lý khách hàng --}}
                <li class="nav-item">
                    <a href="{{ route('customer.view_profile') }}" class="nav-link">
                        <i class="material-icons">account_circle</i>
                        {{ session('ten_khach_hang') }}
                    </a>
                </li>
                
                <li class="nav-item">
                    <a href="{{ route('customer.logout') }}" class="nav-link">
                        <i class="material-icons">power_settings_new</i> Đăng xuất
                    </a>
                </li>
                
            @else

                <li class="nav-item">
                    <a href="{{ route('customer.view_login') }}" class="nav-link">
                        <i class="material-icons">person</i> Đăng nhập
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('customer.view_register') }}" class="nav-link">
                        <i class="material-icons">assignment_ind</i> Đăng ký
                    </a>
                </li>

            @endif
        </ul>
    </div>
</nav>