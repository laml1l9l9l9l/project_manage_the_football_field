<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('favicon.ico') }}">
    <link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <!--     Fonts and icons     -->
    {{-- https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/css-icon.css') }}" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

    <!-- CSS Files -->
    {{-- https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css --}}
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('css/material-kit.css?v=1.2.1') }}" rel="stylesheet"/>

    <style type="text/css">
        .megamenu-li {
            position: static;
        }

        .megamenu {
            position: absolute;
            width: 100%;
            left: 0;
            right: 0;
            padding: 15px;
        }
    </style>

    {{-- extends css --}}
    @stack('css')

</head>

<body class="{{ $class_body }}">
    <!-- Menu -->
    @include('layer.header')

    <!-- Content -->
    @yield('content')

    <!-- Footer -->
    @include('layer.footer')

    <!--   Core JS Files   -->
    <script src="{{ asset('js/jquery-3.3.1.slim.min.js') }}" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="{{ asset('js/material.min.js') }}"></script>
    <script src="{{ asset('js/popper.min.js') }}" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <!--  Plugin for Date Time Picker and Full Calendar Plugin-->
    <script src="{{ asset('js/moment.min.js') }}"></script>

    <!--    Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{ asset('js/nouislider.min.js') }}" type="text/javascript"></script>

    <!--    Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
    <script src="{{ asset('js/bootstrap-datetimepicker.js') }}" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function(){
            var slider = document.getElementById('sliderRegular');

            noUiSlider.create(slider, {
                start: 40,
                connect: [true,false],
                range: {
                    min: 0,
                    max: 100
                }
            });

            var slider2 = document.getElementById('sliderDouble');

            noUiSlider.create(slider2, {
                start: [ 20, 60 ],
                connect: true,
                range: {
                    min:  0,
                    max:  100
                }
            });



            materialKit.initFormExtendedDatetimepickers();

        });
    </script>

    {{-- extends js --}}
    @stack('js')
</body>

</html>
