<footer class="footer footer-default">
	<div class="container">
		<nav class="float-left">
			<ul>
				<li>
					<a href="https://www.facebook.com/laamnguyentung">
						Facebook
					</a>
				</li>
				<li>
					<a href="https://gitlab.com/laml1l9l9l9l">
						About Us
					</a>
				</li>
				<li>
					<a href="">
						Blog
					</a>
				</li>
				<li>
					<a href="">
						Licenses
					</a>
				</li>
			</ul>
		</nav>
		<div class="copyright float-right">
			&copy;
			<script>
				document.write(new Date().getFullYear())
			</script>, made with <i class="material-icons">favorite</i> by
			<a href="https://www.facebook.com/laamnguyentung" target="_blank">Firstking99</a> for a better web.
		</div>
	</div>
</footer>