@extends('layer.master')

@push('css')
	<title>
		Chọn sân đá giải
	</title>
@endpush

@section('content')

	<div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/bg-new-1.jpg') }}');"></div>

    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">

            	<div class="row">
                    <div class="col-xs-6 col-xs-offset-3 mr-auto ml-auto">
                        <h1 class="title">
                            XÁC NHẬN SÂN ĐÁ
                        </h1>
                        <h4 class="text-center">
                        	@if (!empty($array_date))
								ĐẶT GIẢI TRONG NHIỀU NGÀY
							@else
								ĐẶT GIẢI TRONG 1 NGÀY
                        	@endif
                        </h4>
                    </div>
                </div>

                <div class="row">
                	<table class="table table-striped my-5">
                		<thead class="text-center">
                			<tr class="table-primary">
                				<th>
                					Ngày đá
                				</th>
                				<th>
                					Giờ đá
                				</th>
                				<th>
                					Sân có thể đá
                				</th>
                				<th>
                					Giá
                				</th>
                			</tr>
                		</thead>
                		<tbody class="text-center">
                			@for ($i = 0; $i < $tong_so; $i++)
                				<tr>
	                				@if (!empty($array_date))
	                					<td>
	                						{{ $array_date[$i] }}
	                					</td>
	                					<td>
	                						{{ $time->khung_gio }}
	                					</td>
	                				@else
	                					<td>
		                					{{ $ngay_da }}
		                				</td>
		                				<td>
		                					{{ $array_khung_gio[$i] }}
		                				</td>
	                				@endif
	                				<td>
	                					{{ $tong_so }}
	                				</td>
	                				<td>
	                					sdfsd
	                				</td>
	                			</tr>
                			@endfor
                		</tbody>
                	</table>
                </div>

            </div>
        </div>
    </div>
@endsection