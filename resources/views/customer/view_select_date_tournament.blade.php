@extends('layer.master')

@push('css')
    <title>Đặt Sân</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='{{ asset('css/jquery-ui.css') }}' rel='stylesheet' />
    <style type="text/css">

        {{-- Get date --}}
        body {
            background: #f7f6f3;
            font-family: sans-serif;
        }

        /*Button book hour*/
        input[type="checkbox"] {
            display: none;
        }
        input[type="checkbox"]:checked + span {
            background-color: #000000;
        }

        /*Button hidden*/
        .button_hidden{
            display: none;
        }
    </style>
    <link href='{{ asset('css/customer/css-datepicker.css') }}' rel='stylesheet' />
@endpush

@section('content')

<div class="profile-page">
    <div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/bg-new-1.jpg') }}');"></div>

    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">

                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3 mr-auto ml-auto">
                        <h1 class="title">
                            ĐẶT GIẢI TRONG 1 NGÀY
                        </h1>
                    </div>
                </div>


                <div class="description text-center">
                    <p>
                        Bạn hãy chọn khung giờ muốn đá
                    </p>
                </div>

                {{-- quay lại trang chủ xem lịch thuê từng sân --}}
                <div class="text-center">
                    <a href="{{ redirect()->back()->getTargetUrl() }}">
                        <button class="btn btn-info">
                            <i class="fa fa-reply"></i> Quay lại
                        </button>
                    </a>
                </div>

                <div class="tab-content">
                    <div class="text-center" id="thue_ngay">

                        {{-- Thông báo --}}
                        @if (Session::has('error'))
                            <div class="footer text-center alert alert-danger rounded m-2 p-2">
                                <div class="container">
                                    <div class="alert-icon">
                                        <i class="material-icons">error_outline</i>
                                    </div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">
                                            <i class="material-icons">clear</i>
                                        </span>
                                    </button>
                                    {{ Session::get('error') }}
                                </div>
                            </div>
                        @endif

                        <form>
                            @csrf
                            <div class="row">
                                <div class="col-md-4 ml-auto mr-auto mt-5">
                                    <div class="form-group">
                                        <input type="text" name="result_date" class="form-control date_picker text-center" id="ngay_da" value="{{ $ngay_da }}" readonly="readonly">

                                        {{-- Thông báo lỗi --}}
                                        @if ($errors->has('result_date'))
                                            <div class="col-md-12 text-center">
                                                <small class="text-danger">
                                                    {{ $errors->first('result_date') }}
                                                </small>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            
                            <div class="row">

                                <div class="col-md-7 mr-auto ml-auto">
                                    <div class="h2 text-center text-warning">
                                        Khung Giờ Còn Trống
                                    </div>

                                    <div class="form-check">
                                        @foreach ($array_khung_gio as $row_khung_gio)

                                            {{-- Check khung gio dat lich --}}
                                            @php
                                                $check = FALSE;
                                            @endphp

                                            @foreach ($array_check_khung_gio_dat_lich as $row_check)
                                                @if ($row_khung_gio->ma_khung_gio == $row_check->ma_khung_gio && $ma_san_bong == $row_check->ma_san_bong)
                                                    @php
                                                        $check = TRUE;
                                                    @endphp
                                                @else
                                                    @php
                                                        $check = FALSE;
                                                    @endphp
                                                @endif
                                            @endforeach

                                            @if ($check == FALSE)
                                                <label>
                                                    <input type="checkbox" name="khung_gio[]" value="{{ $row_khung_gio->ma_khung_gio }}" class="pick_time">
                                                    <span class="btn btn-lg click_time">{{ $row_khung_gio->khung_gio }}</span>
                                                </label>
                                            @endif
                                            
                                        @endforeach
                                    </div>
                                    {{-- end form check --}}

                                </div>
                            </div>

                            {{-- button submit --}}
                            <div class="row button_hidden" id="submit_form">
                                <div class="col-md-4 mr-auto ml-auto">
                                    <div class="align-self-xl-center">
                                        <button class="btn btn-info" type="button" id="button_submit">
                                            Thuê sân
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                    {{-- end text-center --}}
                </div>
                {{-- end tab-content --}}
            </div>
        </div>
    </div>
</div>

@php
    
@endphp

@endsection

@push('js')
    <script src="{{ asset('js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-ui.js') }}" type="text/javascript"></script>
    
    {{-- format date vietnamese --}}
    <script src="{{ asset('js/format_date_vietnamese.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            // Select date
            $( '.date_picker' ).datepicker({
                dateFormat: "dd/mm/yy"
            });

            // Show button submit
            $( '.pick_time' ).change( function() {
                var date = $( '#ngay_da' ).val();
                if(date != "")
                {
                    $( '#submit_form' ).show();
                }
            });

            $( '#button_submit' ).click(function() {
                $( 'form' ).submit();
            });
        });
    </script>
@endpush
