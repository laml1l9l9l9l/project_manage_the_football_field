@extends('layer.master')

@push('css')
    <title>Đặt Sân</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='{{ asset('css/jquery-ui.css') }}' rel='stylesheet' />
    <style type="text/css">

        {{-- Get date --}}
        body {
            background: #f7f6f3;
            font-family: sans-serif;
        }

        /*Button hidden*/
        .button_hidden{
            display: none;
        }
        
    </style>
@endpush

@section('content')

<div class="profile-page">
    <div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/bg-new-1.jpg') }}');"></div>

    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">

                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3 mr-auto ml-auto">
                        <h1 class="title">
                            ĐẶT LỊCH THUÊ SÂN {{ $ma_san_bong }}
                        </h1>
                    </div>
                </div>


                <div class="description text-center">
                    <p>
                        Bạn hãy chọn khung giờ muốn đá
                    </p>
                </div>

                {{-- quay lại trang chủ xem lịch thuê từng sân --}}
                <div class="text-center">
                    <a href="{{ redirect()->back()->getTargetUrl() }}">
                        <button class="btn btn-info">
                            <i class="fa fa-reply"></i> Quay lại
                        </button>
                    </a>
                </div>

                <div class="tab-content">
                    <div class="text-center" id="thue_ngay">
                        <div class="row">

                            <div class="col-md-7 mr-auto ml-auto">
                                <div class="h2 text-center text-warning">
                                    Khung Giờ Còn Trống
                                </div>
                                @foreach ($array_khung_gio as $row_khung_gio)

                                    {{-- Check khung gio dat lich --}}
                                    @php
                                        $check = FALSE;
                                    @endphp

                                    @foreach ($array_check_khung_gio_dat_lich as $row_check)
                                        @if ($row_khung_gio->ma_khung_gio == $row_check->ma_khung_gio && $ma_san_bong == $row_check->ma_san_bong)
                                            @php
                                                $check = TRUE;
                                            @endphp
                                        @else
                                            @php
                                                $check = FALSE;
                                            @endphp
                                        @endif
                                    @endforeach

                                    @if ($check == FALSE)
                                        @if (strpos($row_khung_gio->khung_gio, 'p'))
                                            <button class="btn btn-lg btn-rose click_time"
                                            value="{{ $row_khung_gio->ma_khung_gio }}"
                                            data-date="{{ $row_khung_gio->khung_gio }}">

                                                {{ $row_khung_gio->khung_gio }}

                                            </button>
                                        @else
                                            <button class="btn btn-lg click_time" value="{{ $row_khung_gio->ma_khung_gio }}"
                                            data-date="{{ $row_khung_gio->khung_gio }}">

                                                {{ $row_khung_gio->khung_gio }}
                                            
                                            </button>
                                        @endif
                                    @endif
                                    
                                @endforeach
                                
                                <div class="mx-auto text-center h3 text-danger print_date"
                                id="result_time"></div>
                            </div>
                        </div>

                        <div class="row button_hidden" id="submit_form">
                            <div class="col-md-4 mr-auto ml-auto">
                                {{-- 
                                    form
                                    Nếu form có đủ ngày và giờ thì submit, tạo session tránh chưa đăng nhập
                                --}}
                                <form>
                                    @csrf
                                    <input type="hidden" name="ma_san_bong" id="get_id">
                                    <input type="hidden" name="ngay_da" id="get_date">
                                    <input type="hidden" name="khung_gio" id="get_time">
                                    <div class="align-self-xl-center">
                                        <button class="btn btn-info" type="button" id="button_submit">
                                            Thuê sân
                                        </button>
                                    </div>
                                </form>
                                {{-- end form --}}
                            </div>
                        </div>
                    </div>
                    {{-- end text-center --}}
                </div>
                {{-- end tab-content --}}
            </div>
        </div>
    </div>
</div>

@php
    
@endphp

@endsection

@push('js')
    <script src="{{ asset('js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-ui.js') }}" type="text/javascript"></script>

    {{-- format date vietnamese --}}
    <script src="{{ asset('js/format_date_vietnamese.js') }}" type="text/javascript"></script>


    <script type="text/javascript">
        // $(document).ready(function() {

        //     // Select date
        //     $('.date_picker').datepicker({
        //         dateFormat: "dd/mm/yy"
        //     });


        //     // Check khung_gio_dat_lich for date selected
        //     $('.date_picker').change(function() {
        //         $.ajax({
        //             url: '{{ route('customer.view_book_football_ground', ['ma_san_bong' => $ma_san_bong]) }}',
        //             type: 'GET',
        //             dataType: 'json',
        //             data: {date: $('.date_picker').val()},
        //         })
        //         .done(function() {
        //             console.log("success");
        //         })
        //         .fail(function() {
        //             console.log("error");
        //         })
        //     });

        // });


        // Get time
        var id = "{{ $ma_san_bong }}";

        var date = "{{ $ngay_da }}";

        var time = "";

        $('.click_time').on('click',function() {
            // Show time clicked
            $('.print_date').text("Thuê khung giờ: " + $(this).attr('data-date') );
            time = $(this).val();

            if((date != "")&&(time != "")){
                $('#submit_form').show();
            }
        });

        $('#button_submit').click(function(){

            $('#get_id').val(id);
            $('#get_date').val(date);
            $('#get_time').val(time);

            $('form').attr('action', '{{ route('customer.view_book_hour_football_ground') }}');
            $('form').attr('method', 'post');
            $('form').submit();
        });
    </script>
@endpush
