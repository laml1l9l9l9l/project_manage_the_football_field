@extends('layer.master')

@push('css')
    <title>Đăng nhập</title>
    <style type="text/css">
        body{
            overflow: hidden;
        }
    </style>
@endpush

@section('content')

<div class="page-header header-filter" style="background-image: url('{{ asset('img/bg-new-banner.jpg') }}'); background-size: cover; background-position: top center;">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 ml-auto mr-auto">
                <div class="card card-signup">

                    {{-- Form --}}
                    <form class="form" method="post" action="{{ route('customer.process_login') }}">
                        <div class="card-header card-header-info text-center">
                            <h4 class="card-title text-dark">Đăng Nhập</h4>
                        </div>
                        
                        {{-- Token --}}
                        @csrf

                        <p class="description text-center">Đăng nhập để có thể thuê sân</p>
                        <div class="card-content">
                            {{-- <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">sentiment_satisfied</i>
                                    </span>
                                </div>
                                <input type="text" class="form-control" placeholder="Tài khoản"
                                name="tai_khoan" value="{{ old('tai_khoan') }}">
                            </div> --}}
                            <div class="input-group mb-3">
                                <span class="input-group-addon">
                                    <i class="material-icons">sentiment_satisfied</i>
                                </span>
                                <input type="text" class="form-control" placeholder="Tài khoản"
                                name="tai_khoan" value="{{ old('tai_khoan') }}">

                                {{-- Thông báo lỗi tài khoản --}}
                                @if ($errors->has('tai_khoan'))
                                    <div class="col-md-12 text-center">
                                        <small class="text-danger">
                                            {{ $errors->first('tai_khoan') }}
                                        </small>
                                    </div>
                                @endif
                            </div>

                            <div class="input-group mb-3">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_open</i>
                                </span>
                                <input type="password" class="form-control" placeholder="Mật khẩu" name="mat_khau" value="{{ old('mat_khau') }}">
                                
                                
                                {{-- Thông báo lỗi mật khẩu --}}
                                @if ($errors->has('mat_khau'))
                                    <div class="col-md-12 text-center">
                                        <small class="text-danger">
                                            {{ $errors->first('mat_khau') }}
                                        </small>
                                    </div>
                                @endif
                            </div>

                        </div>
                        <div class="footer text-center">
                                <button class="btn btn-primary" type="submit">
                                    <i class="material-icons">open_in_new</i> Đăng nhập
                                </button>

                                <br>

                                <a href="{{ route('customer.view_register') }}">
                                    <button class="btn btn-warning btn-round" type="button">
                                        <i class="material-icons">assignment_ind</i> Đăng ký
                                    </button>
                                </a>
                        </div>


                        {{-- Thông báo --}}
                        @if (Session::has('error'))
                            <div class="footer text-center alert alert-danger rounded m-2 p-2">
                                <div class="container">
                                    <div class="alert-icon">
                                        <i class="material-icons">error_outline</i>
                                    </div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">
                                            <i class="material-icons">clear</i>
                                        </span>
                                    </button>
                                    {{ Session::get('error') }}
                                </div>
                            </div>
                        @elseif (Session::has('success'))
                            <div class="footer text-center alert alert-success rounded m-2 p-2">
                                <div class="container">
                                    <div class="alert-icon">
                                        <i class="material-icons">check</i>
                                    </div>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">
                                            <i class="material-icons">clear</i>
                                        </span>
                                    </button>
                                    {{ Session::get('success') }}
                                </div>
                            </div>
                        @endif
                    </form>
                    {{-- End form --}}

                </div>
            </div>
        </div>
    </div>
</div>

@endsection