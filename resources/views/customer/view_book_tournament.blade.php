@extends('layer.master')

@push('css')
    <title>
        Đặt giải đấu
    </title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='{{ asset('css/jquery-ui.css') }}' rel='stylesheet' />
    <style type="text/css">

        {{-- Get date --}}
        body {
            background: #f7f6f3;
            font-family: sans-serif;
        }

        /*Button hidden*/
        .button_hidden{
            display: none;
        }
    </style>
    <link href='{{ asset('css/customer/css-datepicker.css') }}' rel='stylesheet' />
@endpush

@section('content')
	<div class="profile-page">
    <div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/bg-new-1.jpg') }}');"></div>

    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">

                <br>

                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3 mr-auto ml-auto">
                        <h1 class="title">
                            ĐẶT GIẢI TRONG NHIỀU NGÀY
                        </h1>
                    </div>
                </div>


                {{-- Thông báo lỗi --}}
                @if (Session::has('error'))
                    <div class="alert alert-danger rounded m-2 p-2 col-md-11 mr-auto ml-auto">
                        <div class="container">
                            <div class="alert-icon">
                                <i class="material-icons">error_outline</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>

                            {{ Session::get('error') }}
                        </div>
                    </div>
                @endif


                <div class="text-center">
                    <a href="{{ route('customer.home') }}">
                        <button class="btn btn-info">
                            <i class="fa fa-reply"></i> Quay lại
                        </button>
                    </a>
                </div>


                <div class="row">
                    <div class="col-md-6 text-right">
                        <div class="form-group">
                            <h4 class="text-info">
                                Ngày Bắt Đầu
                            </h4>
                            <small>
                                <i>
                                    Bấm để chọn ngày
                                </i>
                            </small>
                            <div class="col-md-6 offset-md-6">
                                <input type="text" name="ngay_bat_dau" class="form-control date_picker text-center" id="ngay_bat_dau" value="{{ $ngay_bat_dau }}" readonly="readonly">

                                {{-- Thông báo lỗi --}}
                                @if ($errors->has('date_start'))
                                    <div class="col-md-12 text-center">
                                        <small class="text-danger">
                                            {{ $errors->first('date_start') }}
                                        </small>
                                    </div>
                                @endif
                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 ml-auto mr-auto">
                        <div class="form-group">
                            <h4 class="text-info">
                                Ngày Kết Thúc
                            </h4>
                            <small>
                                <i>
                                    Bấm để chọn ngày
                                </i>
                            </small>
                            <div class="col-md-6">
                                <input type="text" name="ngay_bat_dau" class="form-control date_picker text-center" id="ngay_ket_thuc" value="{{ $ngay_ket_thuc }}" readonly="readonly">

                                {{-- Thông báo lỗi --}}
                                @if ($errors->has('date_end'))
                                    <div class="col-md-12 text-center">
                                        <small class="text-danger">
                                            {{ $errors->first('date_end') }}
                                        </small>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 text-center mr-auto ml-auto">
                        @foreach ($array_khung_gio as $row_khung_gio)
                            @php
                                $check = FALSE;
                            @endphp
                            
                            {{-- Check khung gio dat lich --}}
                            {{-- @foreach ($array_check_khung_gio_dat_lich as $row_check)
                                @if ($row_khung_gio->ma_khung_gio == $row_check->ma_khung_gio && $row->ma_san_bong == $row_check->ma_san_bong)
                                    <script type="text/javascript">
                                        console.log('true');
                                    </script>
                                    @php
                                        $check = TRUE;
                                    @endphp
                                @else
                                    @php
                                        $check = FALSE;
                                    @endphp
                                @endif
                            @endforeach --}}

                            @if ($check == FALSE)
                                @if (strpos($row_khung_gio->khung_gio, 'p'))
                                    <button class="btn btn-lg btn-rose click_time"
                                    value="{{ $row_khung_gio->ma_khung_gio }}"
                                    data-date="{{ $row_khung_gio->khung_gio }}">

                                        {{ $row_khung_gio->khung_gio }}

                                    </button>
                                @else
                                    <button class="btn btn-lg click_time" value="{{ $row_khung_gio->ma_khung_gio }}"
                                    data-date="{{ $row_khung_gio->khung_gio }}">

                                        {{ $row_khung_gio->khung_gio }}
                                    
                                    </button>
                                @endif
                            @else
                                <script type="text/javascript">
                                    console.log('abc');
                                </script>
                            @endif

                        @endforeach

                        <div class="mx-auto text-center h3 text-danger print_date"
                                id="result_time"></div>
                    </div>
                </div>

                
                {{-- Form book tournament --}}
                <form id="form_book_tournament" class="text-center">
                    @csrf
                    <input type="hidden" name="date_start" id="date_start">
                    <input type="hidden" name="date_end" id="date_end">
                    <input type="hidden" name="time" id="time">
                    <button type="button" id="submit_form" class="btn btn-info button_hidden">
                        Đặt giải
                    </button>
                </form>

            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-ui.js') }}" type="text/javascript"></script>
    
    {{-- format date vietnamese --}}
    <script src="{{ asset('js/format_date_vietnamese.js') }}" type="text/javascript"></script>

	<script type="text/javascript">
        $(document).ready(function() {
            // Select date
            $( '.date_picker' ).datepicker({
                dateFormat: "dd/mm/yy"
            });

            var time = "";

            // Select football ground get all value and submit form
            $( '.click_time' ).click(function() {
                // Show time clicked
                $('.print_date').text("Thuê khung giờ: " + $(this).attr('data-date') );

                time = $(this).val();

                if( time != "" )
                {
                    $('#submit_form').show();
                }
                else
                {
                    $('#submit_form').hide();
                }
            })

            // Click #submit_form
            $( '#submit_form' ).click(function() {
                var date_start = $( '#ngay_bat_dau' ).val();
                var date_end   = $( '#ngay_ket_thuc' ).val();

                var input_date_start = $( '#date_start' ).val(date_start);
                var input_date_end   = $( '#date_end' ).val(date_end);
                var input_time       = $( '#time' ).val(time);

                $( '#form_book_tournament' ).attr('method', 'post');

                if( input_date_start != "" && input_date_end != "" && input_time != "" )
                {
                    $( '#form_book_tournament' ).submit();
                }
            });

            $.ajax({
                url: '{{ route('customer.view_book_tournament') }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: $('input[name="_token"]').val(),
                    ngay_bat_dau: $('#ngay_bat_dau').val(),
                    ngay_ket_thuc: $('#ngay_ket_thuc').val()
                },
            })
            .done(function() {
                console.log("success");
            })
            .fail(function() {
                console.log("error");
            });
            
        });  
    </script>
@endpush