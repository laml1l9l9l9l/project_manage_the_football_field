@extends('layer.master')

@push('css')
	<title>
		Xác nhận thông tin thuê sân
	</title>
@endpush

@section('content')

	<div class="contactus-1 section-image" style="background-image: url('{{ asset('img/bg-view-create-bill.jpg') }}')">

		<div class="container">
			<div class="row">
				
				<div class="col-md-5">
					<h2 class="title">Lưu ý</h2>
					<h5 class="description">
						Bạn kiểm tra lại thông tin đặt sân
					</h5>
					<div class="info info-horizontal">
						<div class="icon icon-primary">
							<i class="material-icons">pin_drop</i>
						</div>
						<div class="description">
							<h4 class="info-title">Địa Chỉ</h4>
							<p> 1 Tạ Quang Bửu,<br>
								Hai Bà Trưng,<br>
								Hà Nội
							</p>
						</div>
					</div>
					<div class="info info-horizontal">
						<div class="icon icon-primary">
							<i class="material-icons">phone</i>
						</div>
						<div class="description">
							<h4 class="info-title">Hotline</h4>
							<p> Quản lý<br>
								0376 221 762<br>
								T2 - T6, 5:00-22:00
							</p>
						</div>
					</div>

				</div>
				<div class="col-md-5 col-md-offset-2">
					<div class="card card-contact">
						<form role="form" method="post" action="{{ route('customer.create_bill_once_football_ground') }}">
							@csrf
							
							<div class="header header-raised header-primary text-center">
								<h4 class="card-title">Thông Tin Người Đặt</h4>
							</div>
							<div class="card-content">


								<div class="form-group label-floating">
									<label class="control-label">Họ và tên</label>
									<input type="text" name="ten_khach_hang" class="form-control" value="{{ $array_khach_hang[0]->ten_khach_hang }}" />

									{{-- Thông báo lỗi tên khách hàng --}}
                                    @if ($errors->has('ten_khach_hang'))
                                        <div class="text-center">
                                            <small class="text-danger">
                                                {{ $errors->first('ten_khach_hang') }}
                                            </small>
                                        </div>
                                    @endif

								</div>


								<div class="form-group label-floating">
									<label class="control-label">Số điện thoại</label>
									<input type="text" name="so_dien_thoai" class="form-control" value="{{ $array_khach_hang[0]->so_dien_thoai }}" />

                                    {{-- Thông báo lỗi tên khách hàng --}}
                                    @if ($errors->has('so_dien_thoai'))
                                        <div class="text-center">
                                            <small class="text-danger">
                                                {{ $errors->first('so_dien_thoai') }}
                                            </small>
                                        </div>
                                    @endif

								</div>


								<div class="form-group label-floating">
									<label class="control-label">Email</label>
									<input type="text" name="email_khach_hang" class="form-control" value="{{ $array_khach_hang[0]->email_khach_hang }}" />

                                    {{-- Thông báo lỗi tên khách hàng --}}
                                    @if ($errors->has('email_khach_hang'))
                                        <div class="text-center">
                                            <small class="text-danger">
                                                {{ $errors->first('email_khach_hang') }}
                                            </small>
                                        </div>
                                    @endif

								</div>


								<div class="row">
									<div class="col-md-12">
										<button type="submit" class="btn btn-primary pull-right">
											<i class="fa fa-paper-plane"></i>
											Xác Nhận
										</button>
									</div>
								</div>

							</div>
							{{-- End card-content --}}

						</form>
					</div>
					{{-- End card card-contact --}}
				</div>

			</div>
		</div>
	</div>

@endsection