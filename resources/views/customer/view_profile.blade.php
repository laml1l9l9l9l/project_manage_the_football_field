@extends('layer.master')

@push('css')
    <title>Quản lý tài khoản</title>
    <style type="text/css">
        .main{
            margin-top: 20px;
        }

        #description{
            margin-bottom: 10px;
        }
    </style>
@endpush

@section('content')
    
    <div class="main main-raised">
        <div class="container">
            <div class="section section-contacts">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto">
                        <h2 class="text-center title">QUẢN LÝ TÀI KHOẢN CÁ NHÂN</h2>
                        <h4 class="text-center description" id="description">Bạn có thể thay đổi, cập nhật thông tin cá nhân tại đây.</h4>


                        {{-- Form thông tin --}}
                        <form class="contact-form" method="post"
                        action="{{ route('customer.update_profile') }}">
                            @csrf
                            
                            <div class="row">

                                {{-- Tên khách hàng --}}
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="bmd-label-floating" for="ho_va_ten">Họ và tên</label>
                                    <input type="text" readonly class="form-control-plaintext" id="ho_va_ten"
                                    value="{{ $array_khach_hang->ten_khach_hang }}">
                                  </div>
                                </div>

                                {{-- Số điện thoại --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="bmd-label-floating" for="so_dien_thoai">Số điện thoại</label>
                                        <input type="text" class="form-control" id="so_dien_thoai" 
                                        value="{{ $array_khach_hang->so_dien_thoai }}"
                                        name="so_dien_thoai">

                                        {{-- Thông báo lỗi tên khách hàng --}}
                                        @if ($errors->has('so_dien_thoai'))
                                            <div class="text-center">
                                                <small class="text-danger">
                                                {{ $errors->first('so_dien_thoai') }}
                                                </small>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            {{-- End row --}}


                            {{-- Email --}}
                            <div class="form-group">
                                <label for="email" class="bmd-label-floating">Email</label>
                                <input type="text" class="form-control" rows="4" id="email"
                                value="{{ $array_khach_hang->email_khach_hang }}"
                                name="email_khach_hang">
                                {{-- Thông báo lỗi tên khách hàng --}}
                                @if ($errors->has('email_khach_hang'))
                                    <div class="text-center">
                                        <small class="text-danger">
                                        {{ $errors->first('email_khach_hang') }}
                                        </small>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-4 ml-auto mr-auto text-center">
                                    <button class="btn btn-primary btn-raised" type="submit">
                                        <i class="fa fa-refresh"></i> Cập nhật
                                    </button>
                                    <a href="{{ route('customer.home') }}">
                                        <button class="btn btn-info btn-raised" type="button">
                                            <i class="fa fa-reply"></i> Quay lại
                                        </button>
                                    </a>
                                </div>
                            </div>

                            {{-- Thông báo --}}
                            @if (Session::has('success'))
                                <div class="footer text-center alert alert-success rounded m-2">
                                    <div class="container">
                                        <div class="alert-icon">
                                            <i class="material-icons">check</i>
                                        </div>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">
                                                <i class="material-icons">clear</i>
                                            </span>
                                        </button>
                                        {{ Session::get('success') }}
                                    </div>
                                </div>
                            @endif
                            
                        </form>
                        {{-- End form --}}

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection