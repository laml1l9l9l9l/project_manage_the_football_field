@extends('layer.master')

@push('css')
    <title>Đăng ký</title>
@endpush

@section('content')
    <div class="page-header header-filter" filter-color="purple" style="background-image: url('{{ asset('img/bg-new-2.jpg') }}'); background-size: cover; background-position: top center;">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-10 col-md-offset-1">
                    
                    <div class="card card-signup">
                        <h2 class="card-title text-center">
                            Đăng Ký
                        </h2>
                        <p class="description text-center">
                            Đăng ký tài khoản để có thể thuê sân
                        </p>
                        <div class="row d-flex justify-content-center">
                            {{-- Form --}}
                            <form class="form" method="post" action="{{ route('customer.process_register') }}">
                                @csrf

                                <table class="table">
                                    <tr>
                                        <td>
                                            {{-- Tài khoản khách hàng --}}
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">sentiment_satisfied</i>
                                                </span>
                                                <input type="text" class="form-control"
                                                placeholder="Tài khoản" name="tai_khoan_khach_hang"
                                                id="tai_khoan_khach_hang" value="{{ old('tai_khoan_khach_hang') }}">
                                            </div>
                                    
                                            {{-- Thông báo lỗi tài khoản --}}
                                            @if ($errors->has('tai_khoan_khach_hang'))
                                                <div class="text-center">
                                                    <small class="text-danger">
                                                        {{ $errors->first('tai_khoan_khach_hang') }}
                                                    </small>
                                                </div>
                                            @endif

                                        </td>
                                        <td>
                                            {{-- Họ và tên --}}
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">face</i>
                                                </span>
                                                <input type="text" class="form-control"
                                                placeholder="Họ và tên" name="ten_khach_hang"
                                                id="ten_khach_hang" value="{{ old('ten_khach_hang') }}">
                                            </div>

                                            {{-- Thông báo lỗi tên khách hàng --}}
                                            @if ($errors->has('ten_khach_hang'))
                                                <div class="text-center">
                                                    <small class="text-danger">
                                                        {{ $errors->first('ten_khach_hang') }}
                                                    </small>
                                                </div>
                                            @endif

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            {{-- Mật khẩu --}}
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">lock_outline</i>
                                                </span>
                                                <input type="password" class="form-control"
                                                placeholder="Mật khẩu" name="mat_khau" id="mat_khau" value="{{ old('mat_khau') }}">
                                            </div>

                                            {{-- Thông báo lỗi tên khách hàng --}}
                                            @if ($errors->has('mat_khau'))
                                                <div class="text-center">
                                                    <small class="text-danger">
                                                        {{ $errors->first('mat_khau') }}
                                                    </small>
                                                </div>
                                            @endif
                                        
                                        </td>
                                        <td>
                                            {{-- Nhập lại mật khẩu --}}
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">lock</i>
                                                </span>
                                                <input type="password" class="form-control"
                                                placeholder="Nhập lại mật khẩu"
                                                name="nhap_lai_ma_khau" id="nhap_lai_ma_khau" value="{{ old('nhap_lai_ma_khau') }}">
                                            </div>

                                            {{-- Thông báo lỗi tên khách hàng --}}
                                            @if ($errors->has('nhap_lai_ma_khau'))
                                                <div class="text-center">
                                                    <small class="text-danger">
                                                        {{ $errors->first('nhap_lai_ma_khau') }}
                                                    </small>
                                                </div>
                                            @endif
                                        
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            {{-- Số điện thoại --}}
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">phone</i>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Số điện thoại" name="so_dien_thoai" id="so_dien_thoai" value="{{ old('so_dien_thoai') }}">
                                            </div>

                                            {{-- Thông báo lỗi tên khách hàng --}}
                                            @if ($errors->has('so_dien_thoai'))
                                                <div class="text-center">
                                                    <small class="text-danger">
                                                        {{ $errors->first('so_dien_thoai') }}
                                                    </small>
                                                </div>
                                            @endif
                                        
                                        </td>
                                        <td>
                                            {{-- Email khách hàng --}}
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="material-icons">email</i>
                                                </span>
                                                <input type="text" class="form-control"
                                                placeholder="Email" name="email_khach_hang"
                                                id="email_khach_hang" value="{{ old('email_khach_hang') }}">
                                            </div>

                                            {{-- Thông báo lỗi tên khách hàng --}}
                                            @if ($errors->has('email_khach_hang'))
                                                <div class="text-center">
                                                    <small class="text-danger">
                                                        {{ $errors->first('email_khach_hang') }}
                                                    </small>
                                                </div>
                                            @endif
                                        
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2">
                                            <div class="footer text-center">
                                                {{-- Nút đăng ký --}}
                                                <button class="btn btn-warning btn-round" type="submit"
                                                id="button_register">
                                                    <i class="material-icons">assignment_ind</i> Đăng ký
                                                </button>
                                            </div>
                                            <div class="footer text-center">
                                                <a href="{{ route('customer.view_login') }}">
                                                    <button class="btn btn-primary" type="button">
                                                        <i class="material-icons">open_in_new</i> Đăng nhập
                                                    </button>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                </table>


                                {{-- Thông báo --}}
                                @if (Session::has('error'))
                                    <div class="footer text-center alert alert-danger rounded m-2 p-2" id="error">
                                        <div class="container">
                                            <div class="alert-icon">
                                                <i class="material-icons">error_outline</i>
                                            </div>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">
                                                    <i class="material-icons">clear</i>
                                                </span>
                                            </button>
                                            <p>
                                                {{ Session::get('error') }}
                                            </p>
                                        </div>
                                    </div>
                                @endif

                                
                            </form>
                            {{-- End form --}}
                        </div>
                    </div>
                    {{-- End card --}}

                </div>
            </div>
        </div>
        {{-- End content --}}
    </div>

@endsection

@push('js')
    {{-- <script type="text/javascript">
        $('#button_register').click(function() {
            var mat_khau          = $('#mat_khau').val();
            var mat_khau_nhap_lai = $('#mat_khau_nhap_lai').val();

            if(mat_khau == mat_khau_nhap_lai)
            {
                $('#button_register').submit();
            }
            else
            {
                alert('sai');
            }

        });
    </script> --}}
@endpush