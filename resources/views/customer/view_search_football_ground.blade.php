@extends('layer.master')

@push('css')
    <title>
        Tìm kiếm sân
    </title>

    <link href='{{ asset('css/fullcalendar.css') }}' rel='stylesheet' />
    <link href='{{ asset('css/fullcalendar.print.css') }}' rel='stylesheet' media='print' />
    <style>
        body {
            text-align: center;
            font-size: 14px;
            font-family: "Helvetica Nueue",Arial,Verdana,sans-serif;
            background-color: #DDDDDD;
            }
    </style>
@endpush

@section('content')

    <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/bg-new-banner.jpg') }}');">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-offset-2 text-center">
                    <h2 class="title">Đặt sân thỏa mãn niềm đam mê</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="main main-raised">
        <div class="container">

            {{-- Danh Sách Sân Bóng Còn Trống Hôm Nay --}}
            <div class="section">
                {{-- Thông báo --}}
                @if (Session::has('error'))
                    <div class="alert alert-danger rounded m-2">
                        <div class="container">
                            <div class="alert-icon">
                                <i class="material-icons">error_outline</i>
                            </div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                            </button>

                            {{ Session::get('error') }}
                        </div>
                    </div>
                @endif

                {{-- quay lại --}}
                <a href="{{ route('customer.home') }}">
                    <button class="btn btn-info">
                        <i class="fa fa-reply"></i> Quay lại
                    </button>
                </a>

                <h3 class="title text-center">Danh Sách Sân Bóng Và Khung Giờ Còn Trống Hôm Nay</h3>
                <hr>

                @foreach ($array_san_bong as $row)

                    <div class="row">

                        <div class="col-md-6">
                            <div class="image mt-1 col-md-12">
                                <img src="{{ asset("storage/$row->anh") }}" alt="Rounded Image" class="img-raised rounded img-fluid">
                            </div>
                        </div>
                        {{-- End col-md-6 --}}

                        <div class="col-md-6">

                            <h2 class="title" id="ten_san_bong">{{ 'Sân số '.$row->ma_san_bong }}</h2>

                            @foreach ($array_khung_gio as $row_khung_gio)
                                @php
                                    $check = TRUE;
                                @endphp
                                
                                {{-- Check khung gio dat lich --}}
                                @foreach ($array_check_khung_gio_dat_lich as $row_check)
                                    @if ($row_khung_gio->ma_khung_gio == $row_check->ma_khung_gio && $row->ma_san_bong == $row_check->ma_san_bong)
                                        @php
                                            $check = FALSE;
                                            break;
                                        @endphp
                                    @endif
                                @endforeach

                                @if ($check == FALSE)
                                    @continue
                                @endif

                                @if (strpos($row_khung_gio->khung_gio, 'p'))
                                    <button class="btn btn-lg btn-rose click_time"
                                    value="{{ $row_khung_gio->ma_khung_gio }}">

                                        {{ $row_khung_gio->khung_gio }}

                                    </button>
                                @else
                                    <button class="btn btn-lg click_time" value="{{ $row_khung_gio->ma_khung_gio }}">

                                        {{ $row_khung_gio->khung_gio }}
                                    
                                    </button>
                                @endif

                            @endforeach

                        </div>
                        {{-- End col-md-6 --}}

                        
                    </div>
                    {{-- End row --}}

                    <hr>

                @endforeach

                {{-- Form create bill --}}
                <form id="form_create_bill" class="invisible" method="post">
                    @csrf
                    <input type="hidden" name="result_date" id="get_date">
                    <input type="hidden" name="result_time" id="get_time">
                    <input type="hidden" name="result_football_ground" id="get_football_ground">
                </form>

                @if (empty($check_ma_san_bong))
                    {{-- quay lại --}}
                    <a href="{{ route('customer.home') }}">
                        <button class="btn btn-info">
                            <i class="fa fa-reply"></i> Quay lại
                        </button>
                    </a>
                @endif

            </div>
            {{-- End section --}}
        </div>
        {{-- End contain --}}

    </div>
    {{-- End main --}}

@endsection

@push('js')
    <script src="{{ asset('js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-ui.custom.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/fullcalendar.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            // Lấy giờ đá
            $('.click_time').click(function(){
                var time = $(this).val();

                var ngay_da = "{{ $ngay_da }}";

                // Lấy ma_san_bong từ thẻ h2.title
                var ma_san_bong = $(this).parent().text();
                var regex = /\d+/;
                ma_san_bong = ma_san_bong.match(regex);

                $('#get_date').val(ngay_da);
                $('#get_time').val(time);
                $('#get_football_ground').val(ma_san_bong);

                $('#form_create_bill').attr('action', '{{ route('customer.create_session_to_create_bill') }}');

                $('#form_create_bill').submit();
            });
        });
    </script>
@endpush