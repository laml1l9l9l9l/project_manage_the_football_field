@extends('layer.master')

@push('css')
    <title>
        Trang chủ
    </title>

    <link href='{{ asset('css/fullcalendar.css') }}' rel='stylesheet' />
    <link href='{{ asset('css/fullcalendar.print.css') }}' rel='stylesheet' media='print' />
    <link href='{{ asset('css/jquery-ui.css') }}' rel='stylesheet' />
    <style>
        body {
            text-align: center;
            font-size: 14px;
            font-family: "Helvetica Nueue",Arial,Verdana,sans-serif;
            background-color: #DDDDDD;
            }

        /*Lịch thuê từng sân*/
        .container {
            padding: 20px;
        }
        input[type="radio"] {
            display: none;
        }
        img {
            padding: 5px;
            margin: 5px;
        }
        input[type="radio"]:checked + img {
            background-color: #aaa;
        }
        
        .modal-backdrop {
            z-index: -1 !important;
        }
    </style>
    <link href='{{ asset('css/customer/css-calendar.css') }}' rel='stylesheet' />
    <link href='{{ asset('css/customer/css-datepicker.css') }}' rel='stylesheet' />
@endpush

@section('content')

    <div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/bg-new-banner.jpg') }}');">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-offset-2 text-center">
                    <h2 class="title">Đặt sân thỏa mãn niềm đam mê</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="main main-raised">
        <div class="container">

            <div class="section">
                    
                    {{-- Thông báo --}}
                    @if (Session::has('error'))
                        <div class="alert alert-danger rounded p-0">
                            <div class="container">
                                <div class="alert-icon">
                                    <i class="material-icons">error_outline</i>
                                </div>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true"><i class="material-icons">clear</i></span>
                                </button>

                                {{ Session::get('error') }}
                            </div>
                        </div>
                    @endif
                    {{-- End thông báo --}}

                <div class="row">

                    <div class="col-md-12 col-md-offset-3 text-center">

                        {{-- Form thuê sân --}}
                        <h3>
                            Tìm Sân
                        </h3>

                        <div class="col-md-8 card card-raised card-form-horizontal">
                            <div class="card-content">
                                <form method="post" id="form_search">

                                    @csrf
                                    
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <input type="text" class="form-control date_picker text-center" value="{{ date('d/m/Y') }}" name="ngay_da" readonly="readonly" />                                    
                                            </div>
                                        </div>
                                        <div class="col-md-3 form-group">
                                            <select class="form-control" name="ten_san_bong">
                                                <option value="">Tên Sân</option>

                                                @foreach ($array_san_bong as $row)
                                                    <option value="{{ $row->ma_san_bong }}">{{ "Sân số ".$row->ma_san_bong }}</option>
                                                @endforeach

                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <button type="submit" class="btn btn-primary" id="search_football_ground">
                                                <i class="fa fa-search"></i> Tìm sân
                                            </button>
                                        </div>

                                        {{-- Nút đặt giải đấu --}}
                                        <div class="col-md-4">
                                            <button type="button" class="btn btn-rose" id="football_ground_league" data-toggle="modal" data-target="#showSelectLeague">
                                                <i class="fa fa-book"></i> Đặt giải đấu
                                            </button>
                                        </div>


                                        <!-- The Modal thông báo cách đặt sân -->
                                        <div class="modal fade" id="showSelectLeague">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                              
                                                    <!-- Modal Header -->
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">
                                                            <b>
                                                                Chọn Cách Đặt Giải Đấu
                                                            </b>
                                                        </h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                
                                                    <!-- Modal body -->
                                                    <div class="modal-body">
                                                        <a href="{{ route('customer.view_select_date_tournament') }}">
                                                            <button class="btn btn-facebook" type="button">
                                                                <i class="fa fa-tag" aria-hidden="true"></i>
                                                                Đặt giải một ngày
                                                            </button>
                                                        </a>
                                                        
                                                        &nbsp

                                                        <a href="{{ route('customer.view_book_tournament') }}">
                                                            <button class="btn btn-twitter"  type="button">
                                                                <i class="fa fa-tags" aria-hidden="true"></i>
                                                                Đặt giải nhiều ngày
                                                            </button>
                                                        </a>
                                                    </div>
                                                
                                                    <!-- Modal footer -->
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-round btn-danger" data-dismiss="modal">Close</button>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- End row --}}

                                    {{-- Thông báo lỗi tài khoản --}}
                                    @if ($errors->has('ngay_da'))
                                        <div class="col-md-12 text-left">
                                            <small class="text-danger">
                                                {{ $errors->first('ngay_da') }}
                                            </small>
                                        </div>
                                    @endif

                                </form>
                            </div>
                        </div>

                        {{-- Lịch thuê sân --}}
                        <h3>
                            Thông Tin Thuê Sân Bóng
                        </h3>

                        <span>
                            <a href="#view_calendar">
                                <button class="btn btn-round" id="button_view_calendar">
                                    Xem Lịch Thuê Sân
                                </button>
                            </a>
                        </span>
                        <span>
                            <a href="#view_football_ground">
                                <button class="btn btn-round btn-warning"
                                id="button_view_football_ground">
                                    Xem Lịch Thuê Từng Sân
                                </button>
                            </a>
                        </span>
                        <hr>

                    </div>
                    {{-- end col-md-12 --}}

                </div>

                {{-- lịch thuê sân của sân bóng --}}
                <div class="row" id="view_calendar">

                    <div id="wrap">
                        <div class="title">
                            <h1>
                                Xem Lịch Thuê Sân
                            </h1>
                        </div>

                        <div id="calendar"></div>

                        <div style="clear:both"></div>
                    </div>

                </div>

                {{-- Chọn sân bóng xem lịch nhiều ngày --}}
                <div class="row" id="view_football_ground" style="display: none;">

                    <div class="col-md-12">
                        <div class="title">
                            <h1>
                                Xem Lịch Thuê Từng Sân
                            </h1>
                        </div>

                        <div class="container">
                            @php
                                // Biến phân biệt id từng form để submit
                                $i = 1;
                            @endphp
                            @foreach ($array_san_bong as $row)
                                <label>
                                    <input type="radio" name="images" value="{{ 'form_'.$i }}" /><img src="{{ asset("storage/$row->anh") }}" width="400" height="300" />
                                </label>
                                
                                {{--
                                    Bấm vào ảnh sẽ đẩy mã ảnh lên url
                                    Lưu ý: đẩy chuẩn form -> đặt form 1 id
                                    Xử lý jquery:
                                        Lấy được id của form khi chọn ảnh
                                --}}
                                <form style="display: none" action="{{ route('customer.view_calendar_detail_football_ground') }}" id="{{ 'form_'.$i }}">
                                    <input type="hidden" name="ma_san_bong" value="{{ $row->ma_san_bong }}">
                                </form>

                                @php
                                    $i++;
                                @endphp
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>

            {{-- Danh Sách Sân Bóng Còn Trống Hôm Nay --}}
            <div class="section">
                <h3 class="title text-center">Đặt Sân Cho Các Ngày Tiếp Theo</h3>
                <hr>

                @foreach ($array_san_bong as $row)

                    <a href="{{ route('customer.view_detail_football_ground',['ma_san_bong' => $row->ma_san_bong]) }}">

                        <div class="row">

                            <div class="col-md-6">
                                <div class="image mt-1 col-md-12">
                                    <img src="{{ asset("storage/$row->anh") }}" alt="Rounded Image" class="img-raised rounded img-fluid">
                                </div>
                            </div>
                            {{-- End col-md-6 --}}

                            <div class="col-md-6">

                                <h2 class="title">{{ 'Sân số '.$row->ma_san_bong }}</h2>

                                <div class="info info-horizontal">
                                    <button class="button_football_ground btn btn-info btn-lg">
                                        Xem Chi Tiết Sân
                                    </button>
                                </div>

                            </div>
                            {{-- End col-md-6 --}}

                            
                        </div>
                        {{-- End row --}}

                    </a>

                    <hr>

                @endforeach

            </div>
            {{-- End section --}}
        </div>
        {{-- End contain --}}

    </div>
    {{-- End main --}}

@endsection

@push('js')
    <script src="{{ asset('js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-ui.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/fullcalendar.js') }}" type="text/javascript"></script>
    
    {{-- format date vietnamese --}}
    <script src="{{ asset('js/format_date_vietnamese.js') }}" type="text/javascript"></script>
            

    <script type="text/javascript">

        {{-- Calendar --}}
        $(document).ready(function() {
            
            // $('#button_view_football_ground').click();
            

            var dateObj   = new Date();
            var momentObj = moment(dateObj);
            
            var start_day = new Date(dateObj.getFullYear(), dateObj.getMonth() - 1, 0);
            var end_day   = new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, 0);
            $('#calendar').fullCalendar({
                height: 1000,
                defaultView: 'month',
                validRange: {
                    start: start_day,
                    end: end_day
                },
                defaultDate: momentObj,
                showNonCurrentDates: true,
                header:{
                    left:   'title',
                    center: 'today prev,next',
                    right:  'month' //agendaWeek,agendaDay - xem theo tuần, ngày
                },
                buttonText: {
                    today: 'Hôm nay',
                    month: 'Lịch Theo Tháng',
                    // agendaDay: 'Lịch Theo Ngày',
                },
                events: {
                    url: '{{ route('customer.load_calendar') }}',
                    type: 'GET'
                },
                monthNames: ['Tháng 1','Tháng 2','Tháng 3','Tháng 4','Tháng 5','Tháng 6','Tháng 7','Tháng 8','Tháng 9','Tháng 10','Tháng 11','Tháng 12'],
                monthNamesShort: ['Th 1','Th 2','Th 3','Th 4','Th 5','Th 6','Th 7','Th 8','Th 9','Th 10','Th 11','Th 12'],
                dayNames:['Chủ Nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'],
                dayNamesShort:['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7']
            });



            // Select date
            $( ".date_picker" ).datepicker({
                dateFormat: "dd/mm/yy"
            });



            // Form search
            // If button search_football_ground click -> action view_search_football_ground
            $('#search_football_ground').click(function () {
                $('#form_search').attr('action','{{ route('customer.view_search_football_ground') }}');
            });
            // If button football_ground_league click -> action view...
            $('#football_ground_league').click(function () {
                $('#form_search').attr('action','{{route('customer.load_calendar')}}');
            });



            // Click button show content
            $('#button_view_calendar').click(function() {
                $('#view_calendar').show();
                $('#view_football_ground').hide();
            });
            $('#button_view_football_ground').click(function() {
                $('#view_football_ground').show();
                $('#view_calendar').hide();
            });

            // Submit form view calendar a football ground
            $('input[type=radio][name=images]').click(function() {
                var value_input_radio = $('input[type=radio][name=images]:checked').val();
                var id_form           = "#";
                var id_form           = id_form.concat(value_input_radio);
                
                $(id_form).submit();
            });
        });

    </script>

@endpush