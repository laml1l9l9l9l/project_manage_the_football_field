@extends('layer.master')

@push('css')
	<title>
		Xem chi tiết sân
	</title>
	<style type="text/css">
		body{
			overflow: hidden;
		}
	</style>
@endpush

@section('content')

	<div class="blogs-1" id="blogs-1">

		<div class="container">

			<div class="row">

				<div class="col-md-12 col-md-offset-1">

					<br>
					<br>
					<br>
					<div class="card card-plain card-blog">
						<div class="row">
							<div class="col-md-5">
								<div class="card-image">
									<img class="img img-raised" src="{{ asset('storage/'.$array_san_bong->anh) }}" />
								</div>
							</div>
							<div class="col-md-7">
								<h6 class="category text-info">Chi Tiết Sân Bóng</h6>
								<br>
								<h3 class="card-title">
									<a href="{{ route('customer.view_book_football_ground', ['ma_san_bong' => $array_san_bong->ma_san_bong]) }}" title="Đặt sân">
										Sân Số {{ $array_san_bong->ma_san_bong }}
									</a>
								</h3>
								<p class="card-description">
									@if ($array_san_bong->loai_san_bong == Config::get('constants.loai_san_bong.san_7'))
										{{ 'Sân 7' }}
									@elseif($array_san_bong->loai_san_bong == Config::get('constants.loai_san_bong.san_9'))
										{{ 'Sân 9' }}
									@elseif($array_san_bong->loai_san_bong == Config::get('constants.loai_san_bong.san_11'))
										{{ 'Sân 11' }}
									@endif
								</p>
								<p class="card-description">
									<a href="{{ route('customer.view_book_football_ground', ['ma_san_bong' => $array_san_bong->ma_san_bong]) }}">
										{{ number_format($array_san_bong->gia)." VNĐ" }}
									</a>
								</p>
								<div>
									<a href="{{ route('customer.home') }}">
										<button class="btn btn-info">
											<i class="fa fa-reply"></i> Quay lại
										</button>
									</a>
									<a href="{{ route('customer.view_book_football_ground', ['ma_san_bong' => $array_san_bong->ma_san_bong]) }}">
										<button class="btn btn-rose">
											<i class="fa fa-bookmark"></i> Đặt sân
										</button>
									</a>
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>
			{{-- End row --}}

		</div>
		{{-- End container --}}


	</div>
	{{-- End blog --}}

	<br>
	<br>
	
@endsection