@extends('layer.master')

@push('css')
	<title>
		Hóa Đơn
	</title>
@endpush

@section('content')
	<div class="page-header header-filter header-small" data-parallax="true" style="background-image: url('{{ asset('img/bg-bill.jpg') }}');">
		<div class="container">
    		<div class="row">
        		<div class="col-md-12 col-md-offset-2 ml-auto mr-auto">
                    <h1 class="title text-center">Hóa Đơn Thuê Sân</h1>
                </div>
            </div>
        </div>
	</div>

	<div class="main main-raised">
		<div class="container">
            <div class="pricing-2">

            	{{-- Thông báo --}}
                @if (Session::has('error'))
    				<div class="alert alert-danger rounded m-2">
			            <div class="container">
							<div class="alert-icon">
								<i class="material-icons">error_outline</i>
							</div>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true"><i class="material-icons">clear</i></span>
							</button>

			            	{{ Session::get('error') }}
			            </div>
			        </div>
                @elseif (Session::has('success'))
    				<div class="alert alert-success rounded m-2">
			            <div class="container">
							<div class="alert-icon">
								<i class="material-icons">check</i>
							</div>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true"><i class="material-icons">clear</i></span>
							</button>

			            	{{ Session::get('success') }}
			            </div>
			        </div>
                @endif
                {{-- End thông báo --}}

    			<div class="row">
    				<div class="col-md-12 col-md-offset-3 ml-auto mr-auto text-center">
						<a href="#hoa_don_thue_theo_ngay" role="tab" data-toggle="tab"
						class="btn btn-raised btn-round btn-rose" id="button_hoa_don_thue_ngay">
							Hóa Đơn Thuê Theo Ngày
						</a>
						<a href="#hoa_don_thue_giai" role="tab" data-toggle="tab"
						class="btn btn-raised btn-round btn-outline-info" id="button_hoa_don_thue_giai">
							Hóa Đơn Thuê Giải
						</a>
    				</div>
                </div>

				<div class="tab-content tab-space">

                    <div class="tab-pane active text-center" id="hoa_don_thue_theo_ngay">
		                <div class="row">

		                	@if ( count($hoa_don) == "0" )
		                		<h3 class="text-warning ml-auto mr-auto mt-5">
		                			{{ 'Bạn chưa có hóa đơn nào, hãy đặt sân để có hóa đơn' }}
		                		</h3>
		                	@else
								@foreach ($hoa_don as $row)
									<div class="col-md-4">
										@if ($row->tinh_trang == "1")
											<div class="card card-pricing card-raised">
												<div class="card-content content-rose">
													<h6 class="category text-info">{{ $row->ten_khach_hang }}</h6>
													<h1 class="card-title">{{ number_format($row->thanh_tien) }} <small>VNĐ</small></h1>
													<ul>
														{{-- Định dạng tình trạng hóa đơn --}}
														@php
															if($row->tinh_trang == "1")
															{
																$class_color = "green";
																$tinh_trang = "Đã thanh toán";
															}
															else if($row->tinh_trang == "0")
															{
																$class_color = "orange";
																$tinh_trang = "Chưa thanh toán";
															}
															else if($row->tinh_trang == "-1")
															{
																$class_color = "brown";
																$tinh_trang = "Đã đặt cọc";
															}
															else if($row->tinh_trang == "-2")
															{
																$class_color = "red";
																$tinh_trang = "Chưa đặt cọc";
															}
														@endphp
														<li><b>Tình trạng hóa đơn</b> <span style="color: {{ $class_color }}">{{ $tinh_trang }}</span></li>


														{{-- Nếu tình trạng đã đặt cọc thì hiển thị ra --}}
														@if ($row->tinh_trang == "-1")
															<li><b>Số tiền đặt cọc</b> {{ number_format($row->tien_dat_coc) }} <small>VNĐ</small></li>
														@endif


														<li><b>Khung giờ đá</b> {{ $row->khung_gio }}</li>

														{{-- Định dạng ngày --}}
														@php
															$ngay_da = date('d-m-Y', strtotime($row->ngay_da));
														@endphp
														<li><b>Ngày đá</b> {{ $ngay_da }}</li>
														<li><b>Số điện thoại</b> {{ $row->so_dien_thoai }}</li>
													</ul>
												</div>
											</div>
										@else
											<div class="card card-pricing card-plain">
												<div class="card-content">
													<h6 class="category text-info">{{ $row->ten_khach_hang }}</h6>
													<h1 class="card-title">{{ number_format($row->thanh_tien) }} <small>VNĐ</small></h1>
													<ul>
														{{-- Định dạng tình trạng hóa đơn --}}
														@php
															if($row->tinh_trang == "1")
															{
																$class_color = "green";
																$tinh_trang = "Đã thanh toán";
															}
															else if($row->tinh_trang == "0")
															{
																$class_color = "orange";
																$tinh_trang = "Chưa thanh toán";
															}
															else if($row->tinh_trang == "-1")
															{
																$class_color = "brown";
																$tinh_trang = "Đã đặt cọc";
															}
															else if($row->tinh_trang == "-2")
															{
																$class_color = "red";
																$tinh_trang = "Chưa đặt cọc";
															}
														@endphp
														<li><b>Tình trạng hóa đơn</b> <span style="color: {{ $class_color }}">{{ $tinh_trang }}</span></li>


														{{-- Nếu tình trạng đã đặt cọc thì hiển thị ra --}}
														@if ($row->tinh_trang == "-1")
															<li><b>Số tiền đặt cọc</b> {{ number_format($row->tien_dat_coc) }} <small>VNĐ</small></li>
														@endif


														<li><b>Khung giờ đá</b> {{ $row->khung_gio }}</li>

														{{-- Định dạng ngày --}}
														@php
															$ngay_da = date('d-m-Y', strtotime($row->ngay_da));
														@endphp
														<li><b>Ngày đá</b> {{ $ngay_da }}</li>
														<li><b>Số điện thoại</b> {{ $row->so_dien_thoai }}</li>
													</ul>
												</div>
											</div>
										@endif
									</div>
								@endforeach
		                	@endif							

						</div>
					</div>
					{{-- end hoa_don_thue_theo_ngay --}}

					<div class="tab-pane text-center" id="hoa_don_thue_giai">
		                <div class="row">

							<div class="col-md-4">
								<div class="card card-pricing card-plain">
									<div class="card-content">
										<h6 class="category text-info">Free</h6>
										<h1 class="card-title"><small>$</small>0<small>/mo</small></h1>
										<ul>
											<li><b>1</b> Project</li>
											<li><b>5</b> Team Members</li>
											<li><b>55</b> Personal Contacts</li>
											<li><b>5.000</b> Messages</li>
										</ul>
										<a href="#pablo" class="btn btn-rose btn-raised btn-round">
											Get Started
										</a>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="card card-pricing card-raised">
									<div class="card-content content-rose">
										<h6 class="category text-info">Premium</h6>
										<h1 class="card-title"><small>$</small>89<small>/mo</small></h1>
										<ul>
											<li><b>500</b> Projects</li>
											<li><b>50</b> Team Members</li>
											<li><b>125</b> Personal Contacts</li>
											<li><b>15.000</b> Messages</li>
										</ul>
										<a href="#abc" class="btn btn-white btn-raised btn-round">
											Get Started
										</a>
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="card card-pricing card-plain">
									<div class="card-content">
										<h6 class="category text-info">Platinum</h6>
										<h1 class="card-title"><small>$</small>199<small>/mo</small></h1>
										<ul>
											<li><b>1000</b> Projects</li>
											<li><b>100</b> Team Members</li>
											<li><b>550</b> Personal Contacts</li>
											<li><b>50.000</b> Messages</li>
										</ul>
										<a href="#def" class="btn btn-rose btn-raised btn-round">
											Get Started
										</a>
									</div>
								</div>
							</div>

						</div>

					</div>
					{{-- end hoa_don_thue_giai --}}

				</div>
				{{-- end tab-content --}}

            </div>

        </div>

    </div>

@endsection

@push('js')
	<script type="text/javascript">
		$('#button_hoa_don_thue_giai').click(function (){
			$('#hoa_don_thue_giai').attr('class', 'tab-pane active text-center');
			$('#hoa_don_thue_theo_ngay').attr('class', 'tab-pane text-center');
		})
		$('#button_hoa_don_thue_ngay').click(function (){
			$('#hoa_don_thue_giai').attr('class', 'tab-pane text-center');
			$('#hoa_don_thue_theo_ngay').attr('class', 'tab-pane active text-center');
		})
	</script>
@endpush