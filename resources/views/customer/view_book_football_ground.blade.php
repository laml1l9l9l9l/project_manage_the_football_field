@extends('layer.master')

@push('css')
    <title>Đặt Lịch Sân {{ $ma_san_bong }}</title>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href='{{ asset('css/jquery-ui.css') }}' rel='stylesheet' />
    <style type="text/css">

        {{-- Get date --}}
        body {
            background: #f7f6f3;
            font-family: sans-serif;
        }

        /*Button hidden*/
        .button_hidden{
            display: none;
        }
    </style>
    <link href='{{ asset('css/customer/css-datepicker.css') }}' rel='stylesheet' />
@endpush

@section('content')

<div class="profile-page">
    <div class="page-header header-filter" data-parallax="true" style="background-image: url('{{ asset('img/bg-new-1.jpg') }}');"></div>

    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">

                <div class="row">
                    <div class="col-xs-6 col-xs-offset-3 mr-auto ml-auto">
                        <h1 class="title">
                            ĐẶT LỊCH THUÊ SÂN {{ $ma_san_bong }}
                        </h1>
                    </div>
                </div>


                <div class="description text-center">
                    <p>
                        Bạn hãy chọn khung giờ muốn đá
                    </p>
                </div>
                <div class="tab-content">
                    <div class="text-center" id="thue_ngay">
                        <div class="row">

                            {{-- Chọn ngày thuê sân --}}
                            <div class="col-md-3 mr-auto ml-auto">
                                <div class="form-group">

                                    {{-- Chọn ngày xong submit form về một hàm xử lý ở controller --}}
                                    <form>
                                        <label class="label-control h2">Chọn ngày: </label>
                                        <input type="text" class="form-control date_picker text-center" id="result_date" value="{{ $date }}"/>
                                    </form>

                                    {{-- Thông báo lỗi --}}
                                    @if ($errors->has('result_date'))
                                        <div class="col-md-12 text-center">
                                            <small class="text-danger">
                                                {{ $errors->first('result_date') }}
                                            </small>
                                        </div>
                                    @endif
                                    
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-7 mr-auto ml-auto">
                                <div class="h2 text-center text-warning">
                                    Khung Giờ Còn Trống
                                </div>
                                @foreach ($array_khung_gio as $row_khung_gio)

                                    {{-- Check khung gio dat lich --}}
                                    @php
                                        $check = TRUE;
                                    @endphp

                                    @foreach ($array_check_khung_gio_dat_lich as $row_check)
                                        @if ($row_khung_gio->ma_khung_gio == $row_check->ma_khung_gio && $ma_san_bong == $row_check->ma_san_bong)
                                            @php
                                                $check = FALSE;
                                                break;
                                            @endphp
                                        @endif
                                    @endforeach

                                    @if ($check == FALSE)
                                        @continue
                                    @endif

                                    @if (strpos($row_khung_gio->khung_gio, 'p'))
                                        <button class="btn btn-lg btn-rose click_time"
                                        value="{{ $row_khung_gio->ma_khung_gio }}"
                                        data-date="{{ $row_khung_gio->khung_gio }}">

                                            {{ $row_khung_gio->khung_gio }}

                                        </button>
                                    @else
                                        <button class="btn btn-lg click_time" value="{{ $row_khung_gio->ma_khung_gio }}"
                                        data-date="{{ $row_khung_gio->khung_gio }}">

                                            {{ $row_khung_gio->khung_gio }}
                                        
                                        </button>
                                    @endif
                                    
                                @endforeach
                                
                                <div class="mx-auto text-center h3 text-danger print_date"
                                id="result_time"></div>
                            </div>
                        </div>

                        <div class="row button_hidden" id="submit_form">
                            <div class="col-md-4 mr-auto ml-auto">
                                {{-- 
                                    form
                                    Nếu form có đủ ngày và giờ thì submit, tạo session tránh chưa đăng nhập
                                --}}
                                <form>
                                    @csrf
                                    <input type="hidden" name="result_date" id="get_date">
                                    <input type="hidden" name="result_time" id="get_time">
                                    <div class="align-self-xl-center">
                                        <button class="btn btn-info" type="button" id="button_submit">
                                            Thuê sân
                                        </button>
                                    </div>
                                </form>
                                {{-- end form --}}
                            </div>
                        </div>
                    </div>
                    {{-- end text-center --}}
                </div>
                {{-- end tab-content --}}
            </div>
        </div>
    </div>
</div>

@php
    
@endphp

@endsection

@push('js')
    <script src="{{ asset('js/jquery-1.10.2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-ui.js') }}" type="text/javascript"></script>

    {{-- format date vietnamese --}}
    <script src="{{ asset('js/format_date_vietnamese.js') }}" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function() {

            // Select date
            $('.date_picker').datepicker({
                dateFormat: "dd/mm/yy"
            });


            // Check khung_gio_dat_lich for date selected
            $('#result_date').change(function() {
                $.ajax({
                    url: '{{ route('customer.ajax_check_book_football_ground') }}',
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        id: {{ $ma_san_bong }},
                        date: $('#result_date').val()
                    },
                })
                .done(function() {
                    console.log("success");
                })
                .fail(function() {
                    console.log("error");
                })
            });

        });


        // Get time
        var date = $('#result_date').val();

        var time = "";

        $('.click_time').on('click',function() {
            // Show time clicked
            $('.print_date').text("Thuê khung giờ: " + $(this).attr('data-date') );

            date = $('#result_date').val();
            time = $(this).val();

            if((date != "")&&(time != "")){
                $('#submit_form').show();
            }
        });

        $('#button_submit').click(function(){

            date = $('#result_date').val();

            $('#get_date').val(date);
            $('#get_time').val(time);

            $('form').submit();
        });
    </script>
@endpush
