@extends('layer.master')

@push('css')
	<title>
		Chọn sân
	</title>
	<style type="text/css">
		/*.div_football_ground{
			width: 50%;
			margin: auto;
		}*/

		/*Set up image*/
		.img-raised{
			object-fit: contain;
		}

		#form_book{
			display: none;
		}
	</style>
@endpush

@section('content')
	<div class="mt-5 section section-examples">
		<div class="container-fluid text-center">
			<div class="row">

				@foreach ($array_san_bong as $row)

					<div class="div_football_ground image mt-1 col-md-6">
						<a href="#selected">
							<img src="{{ asset("storage/$row->anh") }}" alt="Rounded Image"
							class="img-raised rounded img-fluid" style="width: 100%; height: 100%;">
							<button class="button_football_ground btn btn-info btn-lg"
							value="{{ $row->ma_san_bong }}">
								Chọn sân
							</button>
						</a>
					</div>

				@endforeach

				<form method="post" id="form_book_calendar"
				action="{{ route('customer.create_bill') }}">
					@csrf
					<input type="hidden" name="result_date" value="{{ $ngay_da }}">
					<input type="hidden" name="result_time" value="{{ $ma_khung_gio }}">
					<input type="hidden" name="result_football_ground" id="id_football_ground">
				</form>
				{{-- <div class="col-md-6">
					<a href="examples/profile-page.html" target="_blank">
						<img src="{{ asset('img/profile.jpg') }}" alt="Rounded Image"
						class="img-raised rounded img-fluid">
						<button class="btn btn-link btn-primary btn-lg">View Profile Page</button>
					</a>
				</div> --}}
			</div>
		</div>
	</div>

@endsection


@push('js')
	<script type="text/javascript">
		$('a[href="#selected"]').click(function(){
			// get id football ground
			var ma_san_bong = $('.button_football_ground').val();
			alert(ma_san_bong);
			$('#id_football_ground').val(ma_san_bong);

		  	// $('#form_book_calendar').submit();
		}); 
	</script>
@endpush