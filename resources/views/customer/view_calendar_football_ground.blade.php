@extends('layer.master')
@push('css')
    <title>Xem lịch thuê sân số {{ $ma_san_bong }}</title>

	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />


	<link href='{{ asset('css/fullcalendar.css') }}' rel='stylesheet' />
	<link href='{{ asset('css/fullcalendar.print.css') }}' rel='stylesheet' media='print' />
	<style>
		body {
			text-align: center;
			font-size: 14px;
			font-family: "Helvetica Nueue",Arial,Verdana,sans-serif;
			background-color: #DDDDDD;
			}

		#title{
			margin-top: 100px;
		}
	</style>
    <link href='{{ asset('css/customer/css-calendar.css') }}' rel='stylesheet' />
@endpush

@section('content')
<div id='wrap'>
	<div class="title" id="title">
		<h1>
			Xem lịch thuê của sân số {{ $ma_san_bong }}
		</h1>
		<h3>
			Nhấn vào ngày để thuê sân
		</h3>
	</div>
	<div class="row">
		<div class="col-md-3 ml-auto">
			{{-- redirect()->back()->getTargetUrl() --}}
			<a class="nav-link active" href="{{ route('customer.home')."?view=calendar_detail" }}"> 
				<button class="btn btn-info">
					<i class="fa fa-reply"></i> Quay lại
				</button>
			</a>
		</div>
	</div>

	@if (Session::has('error'))
		<div class="alert alert-danger rounded m-2 p-2 col-md-11 mr-auto ml-auto">
            <div class="container">
				<div class="alert-icon">
					<i class="material-icons">error_outline</i>
				</div>
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true"><i class="material-icons">clear</i></span>
				</button>

            	{{ Session::get('error') }}
            </div>
        </div>
    @endif

	<div id='calendar'></div>

	<div style='clear:both'></div>

	
	<form class="invisible" id="form_thue_san">
		@csrf
		<input type="hidden" name="ngay_da" id="ngay_da">
		<input type="hidden" name="ma_san_bong" id="ma_san_bong">
	</form>
</div>

@endsection

@push('js')
	<script src='{{ asset('js/jquery-1.10.2.js') }}' type="text/javascript"></script>
	<script src='{{ asset('js/jquery-ui.custom.min.js') }}' type="text/javascript"></script>
	<script src='{{ asset('js/fullcalendar.js') }}' type="text/javascript"></script>
	<script type="text/javascript">

		$(document).ready(function() {
		    var dateObj   = new Date();
            var momentObj = moment(dateObj);


            var start_day = new Date(dateObj.getFullYear(), dateObj.getMonth() - 1, 0);
            var end_day = new Date(dateObj.getFullYear(), dateObj.getMonth() + 1, 0);

            // Lấy lịch của sân tất cả các ngày
            $('#calendar').fullCalendar({


            	selectable:true,
                height: 1000,
                defaultView: 'month',
                validRange: {
                    start: start_day,
                    end: end_day
                },
                defaultDate: momentObj,
                showNonCurrentDates: true,
                header:{
                    left:   'title',
                    center: 'today prev,next',
                    right:  'month' //agendaWeek,agendaDay - xem theo tuần, ngày
                },
                buttonText: {
                    today: 'Hôm nay',
                    month: 'Lịch Theo Tháng',
                },
                events: {
                    url: '{{ route('customer.load_calendar_detail_football_ground', ['ma_san_bong' => $ma_san_bong]) }}',
                    type: 'GET'
                },
                monthNames: ['Tháng 1','Tháng 2','Tháng 3','Tháng 4','Tháng 5','Tháng 6','Tháng 7','Tháng 8','Tháng 9','Tháng 10','Tháng 11','Tháng 12'],
                monthNamesShort: ['T 1','T 2','T 3','T 4','T 5','T 6','T 7','T 8','T 9','T 10','T 11','T 12'],
                dayNames:['Chủ Nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7'],
                dayNamesShort:['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],


                
                // Get date rent football ground
                dayClick: function(date, jsEvent, view) {
					var day      = date.getDate();
					if (day < 10)
					{
						day = '0' + day;
					}
					var month    = date.getMonth() + 1;
					if (month < 10)
					{
						month = '0' + month;
					}
					var year     = date.getFullYear();
					var rent_day = year + '/' + month + '/' + day;
		            $('#ngay_da').val(rent_day);
		            $('#ma_san_bong').val({{ $ma_san_bong }});

		            $('#form_thue_san').attr('action', '{{ route('customer.view_book_hour_football_ground') }}');
		            $('#form_thue_san').attr('method', 'post');
		            $('#form_thue_san').submit();
		        }


            });

		});

	</script>

@endpush