@extends('admin.layer.master')
@section('content')
<form class="form-horizontal" action="{{ route('admin.process_insert') }}" method="post">
	{{csrf_field()}}
	<div class="form-group">
		<label class="control-label">
			Email:
		</label>
		<input type="text" name="email_admin" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">
			Mật khẩu:
		</label>
		<input type="password" name="mat_khau" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">
			Tên:
		</label>
		<input type="text" name="ten_admin" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">
			Số điện thoại:
		</label>
		<input type="text" name="so_dien_thoai" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">
			Phân quyền:
		</label>
		<select class="form-control" name="phan_quyen">
			<option value="0">Super Admin</option>
			<option value="1">Admin</option>
		</select>
	</div>
	<button class="btn btn-success">
		Thêm
	</button>
	<a href="{{url()->previous()}}">
		<button type="button" class="btn btn-danger">
			Quay lại
		</button>
	</a>
</form>
@endsection
