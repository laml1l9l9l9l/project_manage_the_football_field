@extends('admin.layer.master')
@section('content')
<form class="form-horizontal" action="{{ route('admin.process_update', ['id' => $admin->ma_admin]) }}" method="post">
	{{csrf_field()}}
	<div class="form-group">
		<label class="control-label">
			Email:
		</label>
		<input type="text" name="email_admin" class="form-control" value="{{$admin->email_admin}}">
	</div>
	<div class="form-group">
		<label class="control-label">
			Mật khẩu:
		</label>
		<input type="password" name="mat_khau" class="form-control" value="{{$admin->mat_khau}}">
	</div>
	<div class="form-group">
		<label class="control-label">
			Tên:
		</label>
		<input type="text" name="ten_admin" class="form-control" value="{{$admin->ten_admin}}">
	</div>
	<div class="form-group">
		<label class="control-label">
			Số điện thoại:
		</label>
		<input type="text" name="so_dien_thoai" class="form-control" value="{{$admin->so_dien_thoai}}">
	</div>
	<div class="form-group">
		<label class="control-label">
			Phân quyền:
		</label>
		<select class="form-control" name="phan_quyen">
			<option value="0" @if ($admin->phan_quyen == 0)
				selected 
			@endif>Super Admin</option>
			<option value="1" @if ($admin->phan_quyen == 1)
				selected 
			@endif>Admin</option>
		</select>
	</div>
	<button class="btn btn-success">
		Sửa
	</button>
	<a href="{{url()->previous()}}">
		<button type="button" class="btn btn-danger">
			Quay lại
		</button>
	</a>
</form>
@endsection
