@extends('admin.layer.master')
@if (empty(Session::has('ma_admin')))
	{{ redirect()->route('view_login') }}
@endif
@push('css')
<style type="text/css">
	h1{
		color: red;
	}
</style>
@endpush
@section('content')

<a href="{{ route('admin.view_insert') }}">
	<label>
		<button class="btn btn-fill btn-x-sm">
			<i class="ti-plus"> Thêm mới</i>
		</button>
	</label>	
</a>
<table class="table table-striped ">
	<tr>
		<th>Mã</th>
		<th>Tên</th>
		<th>Email</th>
		<th>Số điện thoại</th>
		<th>Quyền hạn</th>
		<th>Tác vụ</th>
	</tr>
	@foreach ($array_admin as $admin)
		<tr>
			<td>
				{{$admin->ma_admin}}
			</td>
			<td>
				{{$admin->ten_admin}}
			</td>
			<td>
				{{$admin->email_admin}}
			</td>
			<td>
				{{$admin->so_dien_thoai}}
			</td>
			<td>
				@if ($admin->phan_quyen == 0)
					Super Admin
				@else
					Admin
				@endif
			</td>
			<td>
				<a href="{{ route('admin.view_update', ['id' => $admin->ma_admin]) }}" class="btn btn-simple btn-warning btn-icon edit">
					<i class="fa fa-edit"></i>
				</a>
				@if ($admin->ma_admin != Session::get('ma_admin'))
					<a href="{{ route('admin.process_delete', ['id' => $admin->ma_admin]) }}" class="btn btn-simple btn-danger btn-icon remove">
						<i class="fa fa-times"></i>
					</a>
				@endif
			</td>
		</tr>
	@endforeach
</table>
@endsection
@push('js')
<script type="text/javascript">
	
</script>
@endpush
