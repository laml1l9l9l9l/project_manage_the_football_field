@extends('admin.layer.master')
@if (empty(Session::has('ma_admin')))
	{{ redirect()->route('view_login') }}
@endif
@push('css')
<style type="text/css">
	h1{
		color: red;
	}
</style>
@endpush
@section('content')

<table class="table table-striped ">
	<tr>
		
		<th>Tiền đặt cọc</th>
		<th>Thành tiền</th>
		<th>Tình trạng</th>
		<th>Ngày giờ</th>
		<th>Mã khách hàng</th>
	</tr>
	@foreach ($array_hoa_don as $hoa_don)
		<tr>
			
			<td>
				@if($hoa_don->tien_dat_coc == null)
					Chưa đặt cọc
				@else
					{{$hoa_don->tien_dat_coc}}	
				@endif	
			</td>
			<td>
				{{$hoa_don->thanh_tien}}
			</td>
			<td>
				@if ($hoa_don->tinh_trang == 0)
					Chưa thanh toán
				@elseif ($hoa_don->tinh_trang == 1)
					Đã thanh toán
				@elseif ($hoa_don->tinh_trang == -2)
					Chưa đặt cọc
				@else
					Đã đặt cọc
				@endif		
			</td>
			<td>
				{{$hoa_don->ngay_tao}}
			</td>
			<td>
				{{$hoa_don->ma_khach_hang}}
			</td>
			
		</tr>
	@endforeach
</table>
@endsection
@push('js')
<script type="text/javascript">
	
</script>
@endpush
