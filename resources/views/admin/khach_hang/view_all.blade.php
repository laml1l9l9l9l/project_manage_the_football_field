@extends('admin.layer.master')
@if (empty(Session::has('ma_admin')))
	{{ redirect()->route('view_login') }}
@endif
@push('css')
<style type="text/css">
	h1{
		color: red;
	}
</style>
@endpush
@section('content')

<h1>Danh sách khách hàng</h1>
<table class="table table-striped ">
	<tr>
		<th>Mã</th>
		<th>Tài khoản</th>
		<th>Tên</th>
		<th>Email</th>
		<th>Số điện thoại</th>
	</tr>
	@foreach ($array_khach_hang as $khach_hang)
		<tr>
			<td>
				{{$khach_hang->ma_khach_hang}}
			</td>
			<td>
				{{$khach_hang->tai_khoan_khach_hang}}
			</td>
			<td>
				{{$khach_hang->ten_khach_hang}}
			</td>
			<td>
				{{$khach_hang->email_khach_hang}}
			</td>
			<td>
				{{$khach_hang->so_dien_thoai}}
			</td>
			
			{{-- <td>
				<a href="{{ route('admin.view_update', ['id' => $admin->ma_admin]) }}" class="btn btn-simple btn-warning btn-icon edit">
					<i class="fa fa-edit"></i>
				</a>
				<a href="{{ route('admin.process_delete', ['id' => $admin->ma_admin]) }}" class="btn btn-simple btn-danger btn-icon remove">
					<i class="fa fa-times"></i>
				</a>
			</td> --}}
		</tr>
	@endforeach
</table>
@endsection
@push('js')
<script type="text/javascript">
	
</script>
@endpush
