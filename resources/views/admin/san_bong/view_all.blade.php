@extends('admin.layer.master')
@if (empty(Session::has('ma_admin')))
	{{ redirect()->route('view_login') }}
@endif
@push('css')
<style type="text/css">
	h1{
		color: red;
	}
</style>
@endpush
@section('content')
<h1>Danh sách admin</h1>
<a href="{{ route('san_bong.view_insert') }}">
	<label>
		<button class="btn btn-fill btn-x-sm">
			<i class="ti-plus"> Thêm mới</i>
		</button>
	</label>	
</a>
<table class="table table-striped ">
	<tr>
		<th>Mã</th>
		<th>Loại sân</th>
		<th>Ảnh</th>
		<th>Giá</th>
		<th>Tác vụ</th>
	</tr>
	@foreach ($array_san_bong as $san_bong)
		<tr>
			<td>
				{{$san_bong->ma_san_bong}}
			</td>
			<td>
				@if($san_bong->loai_san_bong==1)
					Sân 7
				@elseif($san_bong->loai_san_bong==2)
					Sân 9
				@else
					Sân 11		
				@endif
			</td>
			<td>
				<img src="{{ asset("storage/$san_bong->anh") }}" width="300" height="200">
			</td>
			<td>
				{{ number_format($san_bong->gia)." VNĐ" }}
			</td>
			<td>
				<a href="{{ route('san_bong.view_update', ['id' => $san_bong->ma_san_bong]) }}" class="btn btn-simple btn-warning btn-icon edit" title="Sửa thông tin sân bóng">
					<i class="fa fa-edit"></i>
				</a>
				{{-- <a href="{{ route('admin.process_delete', ['id' => $admin->ma_admin]) }}" class="btn btn-simple btn-danger btn-icon remove">
					<i class="fa fa-times"></i>
				</a> --}}
			</td>
		</tr>
	@endforeach
</table>
@endsection
@push('js')
<script type="text/javascript">
	
</script>
@endpush