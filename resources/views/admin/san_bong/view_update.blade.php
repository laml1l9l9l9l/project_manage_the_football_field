@extends('admin.layer.master')
@section('content')
<form class="form-horizontal" action="{{ route('san_bong.process_update', ['id' => $san_bong->ma_san_bong]) }}" method="post">
	{{csrf_field()}}
	<div class="form-group">
		<label class="control-label">
			Loại Sân bóng:
		</label>
		<select class="form-control" name="loai_san_bong">
			<option value="1" @if ($san_bong->loai_san_bong == "1")
				selected="selected" 
			@endif>Sân 7</option>
			<option value="2" @if ($san_bong->loai_san_bong == "2")
				selected="selected" 
			@endif>Sân 9</option>
			<option value="3" @if ($san_bong->loai_san_bong == "3")
				selected="selected" 
			@endif>Sân 11</option>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">
			Giá:
		</label>
		<input type="text" name="gia" class="form-control" value="{{ number_format($san_bong->gia) }}" id="gia">
	</div>
	<div class="form-group">
		<label class="control-label">
			Ảnh:
		</label>
		<div>
			<img src="{{ asset("storage/$san_bong->anh") }}" width="350" height="200">
		</div>
	</div>
	<button class="btn btn-success">
		Cập nhật
	</button>
	<a href="{{url()->previous()}}">
		<button type="button" class="btn btn-danger">
			Quay lại
		</button>
	</a>
</form>
@endsection

@push('js')
	<script type="text/javascript">
		$(document).ready(function() {
			$("#gia").on({
			    keyup: function() {
			      formatCurrency($(this));
			    },
			    blur: function() { 
			      formatCurrency($(this), "blur");
			    }
			});


			function formatNumber(n) {
			  // format number 1000000 to 1,234,567
			  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
			}


			function formatCurrency(input, blur) {
			  // appends $ to value, validates decimal side
			  // and puts cursor back in right position.
			  
			  // get input value
			  var input_val = input.val();
			  
			  // don't validate empty input
			  if (input_val === "") { return; }
			  
			  // original length
			  var original_len = input_val.length;

			  // initial caret position 
			  var caret_pos = input.prop("selectionStart");
			    
			  // check for decimal
			  if (input_val.indexOf(".") >= 0) {

			    // get position of first decimal
			    // this prevents multiple decimals from
			    // being entered
			    var decimal_pos = input_val.indexOf(".");

			    // split number by decimal point
			    var left_side = input_val.substring(0, decimal_pos);
			    var right_side = input_val.substring(decimal_pos);

			    // add commas to left side of number
			    left_side = formatNumber(left_side);

			    // validate right side
			    right_side = formatNumber(right_side);
			    
			    // On blur make sure 2 numbers after decimal
			    if (blur === "blur") {
			      right_side += "00";
			    }
			    
			    // Limit decimal to only 2 digits
			    right_side = right_side.substring(0, 2);

			    // join number by .  Thêm dấu "." để có số thập phân
			    input_val = left_side + "" + right_side;

			  } else {
			    // no decimal entered
			    // add commas to number
			    // remove all non-digits
			    input_val = formatNumber(input_val);
			    input_val = input_val;
			    
			    // final formatting
			    if (blur === "blur") {
			      input_val += "";
			    }
			  }
			  
			  // send updated string to input
			  input.val(input_val);

			  // put caret back in the right position
			  var updated_len = input_val.length;
			  caret_pos = updated_len - original_len + caret_pos;
			  input[0].setSelectionRange(caret_pos, caret_pos);
			}
		});
	</script>
@endpush