@extends('admin.layer.master')
@section('content')
<form class="form-horizontal" action="{{ route('san_bong.process_insert') }}" enctype="multipart/form-data" method="post">
	{{csrf_field()}}
	<div class="form-group">
		<label class="control-label">
			Loại Sân bóng:
		</label>
		<select class="form-control" name="loai_san_bong">
			<option value="1">Sân 7</option>
			<option value="2">Sân 9</option>
			<option value="3">Sân 11</option>
		</select>
	</div>
	<div class="form-group">
		<label class="control-label">
			Giá:
		</label>
		<input type="text" name="gia" class="form-control">
	</div>
	<div class="form-group">
		<label class="control-label">
			Ảnh:
		</label>
		<input type="file" name="anh" class="form-control" accept="image/*">
	</div>
	<button class="btn btn-success">
		Thêm
	</button>
	<a href="{{url()->previous()}}">
		<button type="button" class="btn btn-danger">
			Quay lại
		</button>
	</a>
</form>
@endsection
