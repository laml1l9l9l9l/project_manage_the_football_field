<div class="sidebar" data-background-color="brown" data-active-color="danger" data-image="{{ asset('img/avt/default-avatar.png') }}">
	<div class="logo">
		<a href="#" class="simple-text logo-mini">
			Ad
		</a>

		<a href="#" class="simple-text logo-normal" style="position: absolute; left: 53px">
			Administrator
		</a>
	</div>
	<div class="sidebar-wrapper">
		<div class="user">
            <div class="photo">
                <img src="{{ asset('img/avt/'.Session::get('anh_admin').'') }}" width="100%" height="100%" />
            </div>
            <div class="info">
				<a data-toggle="collapse" class="collapsed">
                    <span>
						{{ Session::get('ten_admin') }}	
					</span>
                </a>
				<div class="clearfix"></div>

                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li>
							<a href="#profile">
								<span class="sidebar-mini">Mp</span>
								<span class="sidebar-normal">My Profile</span>
							</a>
						</li>
                        <li>
							<a href="#edit">
								<span class="sidebar-mini">Ep</span>
								<span class="sidebar-normal">Edit Profile</span>
							</a>
						</li>
                        <li>
							<a href="#settings">
								<span class="sidebar-mini">S</span>
								<span class="sidebar-normal">Settings</span>
							</a>
						</li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
			<li>
                <a href="{{ route('admin.view_all') }}">
                    <i class="ti-user"></i>
                    <p>Quản lý admin
                        
                    </p>
                </a>
            </li>
            <li>
                <a href="{{ route('san_bong.view_all') }}">
                    <i class="ti-basketball"></i>
                    <p>Quản lý sân bóng
                        
                    </p>
                </a>
            </li>
            <li>
                <a href="{{ route('khach_hang.view_all') }}">
                    <i class="ti-wallet"></i>
                    <p>Quản lý khách hàng
                        
                    </p>
                </a>
            </li>
            <li>
                <a href="{{ route('hoa_don.view_all') }}">
                    <i class="ti-agenda"></i>
                    <p>Quản lý hóa đơn
                        
                    </p>
                </a>
            </li>
            <li>
                <a href="{{ route('Calendar') }}">
                    <i class="ti-calendar"></i>
                    <p>Lịch
                        
                    </p>
                </a>
            </li>
		</ul>		
	</div>
</div>