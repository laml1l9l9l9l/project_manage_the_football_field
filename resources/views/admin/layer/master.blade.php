<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/apple-icon.png') }}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('img/apple-icon.png') }}">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Paper Dashboard PRO by Creative Tim</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('css/admin/bootstrap.min.css') }}" rel="stylesheet" />

    <!--  Paper Dashboard core CSS    -->
    <link href="{{ asset('css/admin/paper-dashboard.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/admin/themify-icons.css') }}" rel="stylesheet"/>

    <!--  Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>

    {{-- <style type="text/css">
    	body{
    		overflow: auto !important;
    	}
    </style> --}}
    
</head>

<body>
	<div class="wrapper">
	    <!--Menu-->
		@include('admin.layer.menu')

	    <div class="main-panel">
	        <!--Header-->
			@include('admin.layer.header')

	        <div class="content">
	            <div class="container-fluid">
	                <div class="row">
	                    <div class="col-lg-12">
	                        <!--Content-->
	                        @yield('content')
	                    </div>
	                </div>
	            </div>
	        </div>
			<footer class="footer">
			<!--Footer-->
			@include('admin.layer.footer')
	        </footer>
	    </div>
	</div>
</body>

	<!--   Core JS Files. Extra: TouchPunch for touch library inside jquery-ui.min.js   -->
	<script src="{{ asset('js/admin/jquery-3.1.1.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/admin/jquery-ui.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/admin/perfect-scrollbar.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('js/admin/bootstrap.min.js') }}" type="text/javascript"></script>

	
	<!-- Paper Dashboard PRO Core javascript and methods for Demo purpose -->
	<script src="{{ asset('js/admin/paper-dashboard.js') }}"></script>
	<script src="{{ asset('js/admin/fullcalendar.min.js') }}"></script>
	<script src="{{ asset('js/admin/moment.min.js') }}"></script>
	<script src="{{ asset('js/admin/bootstrap-datetimepicker.js') }}"></script>
	<script src="{{ asset('js/admin/bootstrap-selectpicker.js') }}"></script>

	@stack('js')

	
</html>
